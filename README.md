# Archport

Archport is a set of drush commands that can export and import Drupal nodes and their associated fields, files, tags,
and user information.

You can use it as is for fairly simple sites, but you may want to fork the code and adjust it to fit your sites specific
needs.

Why another module for importing and exporting? Partially a bad case of not invented here syndrome, but also because the
other modules did not work when I tried them. I was spending enough time just figuring out how to work around their
quirks, that I figured I might as well have more fun and write my own.

## Similar Modules

* [Node Export](https://drupal.org/project/node_export)
* [Feeds](https://drupal.org/project/feeds)
* Drupal and Drush's built-in migration tools.
  See [the docs](https://www.drupal.org/docs/upgrading-drupal/upgrading-from-drupal-6-or-7-to-drupal-9-or-later).

## Install

Install as you would any other module.

> Note: There are no web ui elements to this module.

### Drush

When choosing which version of Drush to install, be sure to reference their support
matrix: https://www.drush.org/latest/install/#drupal-compatibility

#### Drupal 7

I developed the Drupal 7 version using Drush 8.4.10. See: https://github.com/drush-ops/drush/releases/tag/8.4.10

Any newer versions of Drush 8 should work as well.

#### Drupal 9

I used the latest stable version installed via `composer require drush/drush`.

## How it works

Archport is fairly simple. It just loads the node, then parses out the loaded data into an array. The array is then
saved as json into a subdirectory based on the node's type and id. Any associated files are also copied into that
subdirectory.

The resulting data structure looks something like this:

```
archport/
-- settings.json
-- map.json
-- < content type >/
  -- < node id >/
    -- data.json
    -- files/
      -- public (and/or private, and/or unknown)
        -- < relative path to file >
```

After that, you transfer that data to someplace Drush can access both it and the target site (including the ability to
move files into the target sites public or private folders.)

Then you import the data into your target site.

If all goes well, your node(s) are imported, files are copied and managed, users are imported, and any vocabularies and
terms are imported.

## Limitations

* The Drupal 7 Media module is not supported.
* Term hierarchies are not supported. Imported terms will be imported at the base level. (This will likely be fixed if a
  future project occurs.)
* If multiple nodes reference the same file, that file will be imported multiple times.
* If you import a node more than once, you will end up with more than one node.
* If you import a node more than once, and are importing aliases, you will end up with duplicate aliases.
* Users are imported without passwords and are blocked.
* Comments are not supported.
* Text formats are not mapped. This will be fixed later.

### Field Types

Only a few field types are currently supported.

* Text
* Image
* File
* Term
* Media (Drupal 9 only.)

## Step-by-step instructions

* Install Archport on both source and target sites.
  * Decide where the Archport directory will live. I recommend `private://archport`, and will be using that in my
    example commands.
* Make sure your target site content types have the same field types as the source site.
  * So, if, on your source site, you have a text field called "Author Summary" on a Web Log content type, make sure your
    target site has a text field on the content type you want to import to for that data.
* On the source site:
  * Run the appropriate initialize command:
    * Drupal 7: `archport-initialize private://archport`
    * Drupal 9: `archport:initialize private://archport`
  * Edit `private://archport/settings.json` and adjust anything you need to adjust.
  * Edit ` private://archport/map.json` and set the appropriate field type for the `field_types` array.
    * Normal text fields should be `text`.
    * Basic image fields should be `image`.
    * Taxonomy fields should be `term`.
    * File fields should be `file`.
    * Media fields should be `media`. (Drupal 9 only.)
  * Run the export command that best fits what you want to export.
    * A single node:
      * Drupal 7: `archport-export-single-node private://archport 1234` (Where 1234 is the node id.)
      * Drupal 9: `archport:export-single-node private://archport 1234` (Where 1234 is the node id.)
    * An entire content type:
      * Drupal 7: `archport-export-content-type private://archport page` (Where page is the machine name of the content
        type.)
      * Drupal 9: `archport:export-content-type private://archport page` (Where page is the machine name of the content
        type.)
    * All nodes:
      * Drupal 7: `archport-export-all-nodes private://archport`
      * Drupal 9: `archport:export-all-nodes private://archport`
* Transfer the `private://archport` directory to `private://archport` on your target site. Make sure the data is owned
  by the server user.
* One your target site:
  * Run the appropriate generate map command to gather content types and field names from the target site:
    * Drupal 7: `archport-generate-map private://archport`
    * Drupal 9: `archport:generate-map private://archport`
  * Edit `private://archport/map.json`.
    * Map the source content type to the target content type using the `content_types` array.
    * Set the type for any new fields in the `field_types` array.
    * Map source fields to target fields using the `fields` array.
    * For each exported user in the `users` array, map them to a target user if you have one. Otherwise, leave
      the `target_uid` blank if you will be importing them, or set it to `0` if you want their content owned by
      anonymous.
    * For each exported vocabulary in the `vocabularies` array, map them to their target vocabulary if you have one.
      Otherwise, you can leave the `target_` fields blank, and they will be imported.
    * For each exported term in the `terms` array, map them to their target term if you have one. Otherwise, you can
      leave the `target_` fields blank, and they will be imported.
  * If you are importing users, run the import users command.
    * Drupal 7: `archport-import-users private://archport`
    * Drupal 9: `archport:import-users private://archport`
  * Run the import command that best fits what you are importing.
    * A single node:
      * Drupal 7: `archport-import-single-node private://archport 1234` (Where 1234 is the node id from the source
        site.)
      * Drupal 9: `archport:import-single-node private://archport 1234` (Where 1234 is the node id from the source
        site.)
    * An entire content type:
      * Drupal 7: `archport-import-content-type private://archport page` (Where page is the machine name of the content
        type from the source site.)
      * Drupal 9: `archport:import-content-type private://archport page` (Where page is the machine name of the content
        type from the source site.)
    * All nodes:
      * Drupal 7: `archport-import-all-nodes private://archport`
      * Drupal 7: `archport:import-all-nodes private://archport`

## Enabling Redirect Rule Generation

Archport can generate Apache or HAProxy redirect rules for any nodes you import. Just set `source_site_base_url`
and `target_site_base_url`, to appropriate urls in settings.json. The set `write_apache_redirects`
and/or `write_haproxy_redirects` to `true`.

The rules will be written to `< archport_directory >/apache_redirects.txt`
and/or `< archport_directory >/haproxy_redirects.txt`.

It is up to you to verify them and add them to your server configuration.

## Settings

The default `private://archport/settings.json` file looks like:

```json
{
  "source_site_base_url": "",
  "target_site_base_url": "",
  "write_apache_redirects": false,
  "write_haproxy_redirects": false,
  "keep_created": true,
  "keep_updated": true,
  "skip_vocabularies": false,
  "skip_terms": false,
  "create_users": true,
  "create_vocabularies": true,
  "create_terms": true,
  "create_aliases": true,
  "create_nid_aliases": false,
  "override_file_paths": {
    "public": {
      "override": false,
      "existing_action": "rename",
      "path": "public:\/\/"
    },
    "private": {
      "override": false,
      "existing_action": "rename",
      "path": "private:\/\/"
    },
    "unknown": {
      "override": false,
      "existing_action": "rename",
      "path": "private:\/\/unknown"
    }
  }
}
```

> Warning about `create_nid_aliases`: If you use this, you could very well end up having conflicts with existing, or new
> nodes. So I highly recommend only using it on a brand-new target site, and that you manually set the appropriate auto
> increment counter for nid's in the database to a number higher than your source sites newest node.

### override_file_paths

Normally files are saved to the same relative path as they had in the source site. This setting lets you put all
imported files into a single directory, based on their scheme.

### keep_updated

This setting will likely make no real difference. It will likely be removed later.
