<?php
/** @file Archport functions dealing specifically with nodes. */

namespace Drupal\archport\Includes;

use Drupal;
use Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException;
use Drupal\Component\Plugin\Exception\PluginNotFoundException;
use Drupal\Core\Entity\EntityStorageException;
use Drupal\Core\Language\LanguageInterface;
use Drupal\node\Entity\Node;

class NodeUtilities {

  /**
   * Read a node's data from its data.json file.
   *
   * @param string $archport_directory
   * @param int $id
   * @param string $type
   *
   * @return false|mixed
   */
  public static function _archport_read_node_data(
    string $archport_directory,
    int $id,
    string $type
  ): mixed {
    if (file_exists(
      $archport_directory . '/' . $type . '/' . $id . '/data.json'
    )) {
      return json_decode(
        file_get_contents(
          $archport_directory . '/' . $type . '/' . $id . '/data.json'
        ),
        TRUE
      );
    }
    else {
      echo t(
          "Archport - read_node_data - Could not find node data for type and id."
        ) . "\n";
    }
    return FALSE;
  }

  /**
   * Try to read 'users' item from $data.
   *
   * @param array $data
   *
   * @return bool|array
   */
  public static function _archport_get_node_users_from_data(array $data
  ): array|bool {
    if (!isset($data['users'])) {
      echo t(
        "Archport - get_node_users_from_data - \$data['users'] was not set.\n"
      );
      return FALSE;
    }
    else {
      return $data['users'];
    }
  }

  /**
   * Try to read the node content type from $data.
   *
   * @param array $data
   *
   * @return bool|string
   */
  public static function _archport_get_node_content_type_from_data(array $data
  ): bool|string {
    if (isset($data['node']['type'])) {
      return $data['node']['type'];
    }
    else {
      echo t(
        "Archport - get_node_content_type_from_data - \$data['node']['type'] was not set.\n"
      );
      return FALSE;
    }
  }

  /**
   * Find any fields in $data. Image, File, Taxonomy, or other.
   *
   * @param array $data
   *
   * @return array
   */
  public static function _archport_get_node_fields_from_data(array $data
  ): array {
    $fields = [];
    if (!empty($data['fields'])) {
      foreach ($data['fields'] as $field) {
        $fields[] = $field['field_name'];
      }
    }
    if (!empty($data['files'])) {
      foreach ($data['files'] as $file) {
        $fields[] = $file['field_name'];
      }
    }
    if (!empty($data['taxonomies'])) {
      foreach ($data['taxonomies'] as $taxonomy) {
        foreach ($taxonomy['terms'] as $term) {
          if (!in_array($term['field_name'], $fields)) {
            $fields[] = $term['field_name'];
          }
        }
      }
    }
    return $fields;
  }

  /**
   * Process and import a node.
   *
   * @param string $import_dir
   * @param array $data
   * @param array $settings
   * @param array $map
   *
   * @return bool
   */
  public static function _archport_import_node(
    string $import_dir,
    array $data,
    array $settings,
    array $map
  ): bool {
    $content_type_map = $map['content_types'];
    $target_type = $content_type_map[$data['node']['type']];
    echo t(
        "Archport - import_node - Node title is: "
      ) . $data['node']['title'] . "\n";
    echo t("Archport - import_node - target_type is: ") . $target_type . "\n";

    // Create vocabularies and terms first.
    // Loop over any taxonomy data and add terms where appropriate.
    try {
      /** @var \Drupal\taxonomy\VocabularyStorage $vocabularyStorage */
      $vocabularyStorage = \Drupal::entityTypeManager()->getStorage(
        'taxonomy_vocabulary'
      );
    } catch (InvalidPluginDefinitionException|PluginNotFoundException $e) {
      echo t(
          'Failed to get entity storage class. Exception: '
        ) . $e->getMessage() . "\n";
      return FALSE;
    }
    try {
      /** @var \Drupal\taxonomy\TermStorage $termStorage */
      $termStorage = \Drupal::entityTypeManager()->getStorage('taxonomy_term');
    } catch (InvalidPluginDefinitionException|PluginNotFoundException $e) {
      echo t(
          'Failed to get entity storage class. Exception: '
        ) . $e->getMessage() . "\n";
      return FALSE;
    }
    // Initialize the new node array before we use it.
    $new_node = [];
    foreach ($data['taxonomies'] as $vocabulary) {
      // We import vocabularies before we try to import nodes, so this vocab
      // should exist already.
      // Apparently vocabularies can be exported without any data other than a
      // field_name value. Skip them.
      if (empty($vocabulary['machine_name'])) {
        continue;
      }
      $target_vocabulary = $vocabularyStorage->load(
        $vocabulary['machine_name']
      );
      if (empty($target_vocabulary)) {
        echo t(
            "Archport - import_node - Failed to load vocabulary by machine name when importing taxonomy data: "
          ) . $vocabulary['machine_name'] . "\n";
        return FALSE;
      }
      foreach ($vocabulary['terms'] as $term_data) {
        // We import terms before we import nodes, so this term should exist already.
        // So we need to load it.
        $target_term_load = $termStorage->loadByProperties(
          [
            'name' => $term_data['name'],
            'vid' => $target_vocabulary->get('vid'),
          ]
        );
        if (empty($target_term_load)) {
          echo t('Archport - import_node - Failed to load term by name when importing taxonomy data, the load result was empty. Vocabulary: ') . $target_vocabulary->get('vid') . t(' Term: ') . $term_data['name'] . "\n";
          return FALSE;
        }
        if (count($target_term_load) > 1) {
          echo t(
              'Archport - import_node - Found multiple terms with the same name in the same vocabulary. You should manually fix this. Vocabulary: ') . $target_vocabulary->get('vid') . t(' Term: ') . $term_data['name'] . "\n";
        }
        // Drupal returns an array indexed by tid. At this point we know there
        // is only the one term in the array, so just pop it off.
        $target_term = array_pop($target_term_load);
        if (empty($target_term)) {
          echo t('Archport - import_node - Failed to load term by name when importing taxonomy data: Vocabulary: ') . $target_vocabulary->get('vid') . t(' Term: ') . $term_data['name'] . "\n";
          return FALSE;
        }
        $target_field_name = $map['fields'][$term_data['field_name']];
        /** @var \Drupal\taxonomy\Entity\Term $target_term */
        $new_node[$target_field_name][] = [
          'target_id' => $target_term->id(),
        ];
      }
    }

    if (!empty($map['users'][$data['node']['uid']]['target_uid'])) {
      $target_uid = $map['users'][$data['node']['uid']]['target_uid'];
    }
    else {
      $target_uid = 0;
    }
    $new_node['is_new'] = TRUE;
    $new_node['type'] = $target_type;
    $new_node['bundle'] = 'node';
    $new_node['langcode'] = LanguageInterface::LANGCODE_NOT_SPECIFIED;

    if (!empty($data['node']['body'])) {
      // There should only ever be one body value.
      // So it should be safe to use [0] here.
      $body['value'] = $data['node']['body'];
      $body['format'] = $data['node']['body_format'];

      if (!empty($data['node']['body_summary'])) {
        $body['summary'] = $data['node']['body_summary'];
      }
      $new_node['body'] = $body;
    }

    if (!empty($data['node']['status'])) {
      $new_node['status'] = $data['node']['status'];
    }
    if (!empty($data['node']['title'])) {
      $new_node['title'] = $data['node']['title'];
    }
    $new_node['uid'] = (int) $target_uid; // After node_object_prepare, otherwise it gets overwritten.
    if ($settings['keep_created']) {
      $new_node['created'] = (int) $data['node']['created'];
    }
    // Drupal changes the updated date no matter what.
    foreach ($data['fields'] as $source_field) {
      $target_field_name = $map['fields'][$source_field['field_name']];
      $field_data = [];
      if (isset($source_field['format'])) {
        $field_data['format'] = $source_field['format'];
      }
      if (isset($source_field['value'])) {
        $field_data['value'] = $source_field['value'];
      }
      if (!empty($field_data)) {
        $new_node[$target_field_name][] = $field_data;
      }
    }

    // Loop over any files that we need to process.
    foreach ($data['files'] as $source_file) {
      // Add the file as a managed file in the correct destination
      $file = FileUtilities::_archport_import_file(
        $import_dir,
        $source_file,
        $settings
      );
      if (!$file) {
        // The file public static functions will have output helpful messages.
        return FALSE;
      }
      /** @var \Drupal\file\Entity\File $file */
      $file_as_array['target_id'] = $file->id();
      // Check if the display property exists, add it if it does.
      if (!empty($source_file['display'])) {
        $file_as_array['display'] = $source_file['display'];
      }
      // Check if the description property exists, add it if it does.
      if (!empty($source_file['description'])) {
        $file_as_array['description'] = $source_file['description'];
      }
      // Check if the alt property exists, add it if it does.
      if (!empty($source_file['alt'])) {
        $file_as_array['alt'] = $source_file['alt'];
      }
      // Check if the title property exists, add it if it does.
      if (!empty($source_file['title'])) {
        $file_as_array['title'] = $source_file['title'];
      }

      // Get the target node's field name for this file.
      $target_field = $map['fields'][$source_file['field_name']];
      // Add the file to the node in the target field.
      $new_node[$target_field][] = $file_as_array;
    }

    if (!empty($data['media'])) {
      // Import media items
      $media_items = $data['media'];
      foreach ($media_items as $media_item) {
        $imported = MediaUtilities::_archport_import_media_item(
          $import_dir,
          $media_item,
          $map,
          $settings
        );
        if (!$imported) {
          return FALSE;
        }
        else {
          $new_node[$media_item['media_field_name']][] = [
            'target_id' => $imported->id(),
          ];
        }
      }
    }

    try {
      /** @var \Drupal\node\NodeStorage $nodeStorage */
      $nodeStorage = \Drupal::entityTypeManager()->getStorage('node');
    } catch (InvalidPluginDefinitionException|PluginNotFoundException $e) {
      echo t(
          'Failed to get entity storage class. Exception: '
        ) . $e->getMessage() . "\n";
      return FALSE;
    }

    $create = $nodeStorage->create($new_node);
    $create->enforceIsNew();
    try {
      $create->save();
      if ($settings['create_nid_aliases']) {
        NodeUtilities::_archport_create_nid_alias($create, $data);
      }
      // Run the old alias import second so it becomes the default.
      if ($settings['create_aliases']) {
        NodeUtilities::_archport_import_node_alias($create, $data);
      }
      $archport_directory = Utilities::_archport_get_archport_directory_from_uri(
        $import_dir
      );
      if (!empty($settings['write_apache_redirects'])) {
        NodeUtilities::_archport_write_redirect_line(
          $archport_directory,
          'apache',
          $settings,
          $create,
          $data
        );
      }
      if (!empty($settings['write_haproxy_redirects'])) {
        NodeUtilities::_archport_write_redirect_line(
          $archport_directory,
          'haproxy',
          $settings,
          $create,
          $data
        );
      }
      return TRUE;
    } catch (EntityStorageException $e) {
      echo t(
          'Archport - import_node - Failed to save new node. Exception: '
        ) . $e->getMessage() . "\n";
      return FALSE;
    }
  }

  /**
   * Add the alias from the old node to the newly imported node.
   * Be sure to check if $settings['create_aliases'] is true before calling.
   *
   * @param \Drupal\node\Entity\Node $created_node The new node returned by the
   *                                               create() method.
   * @param array $node_data The data from the old node's
   *                                               data.json file.
   *
   * @return bool
   */
  public static function _archport_import_node_alias(
    Node $created_node,
    array $node_data
  ): bool {
    // Add the old alias
    /** @var \Drupal\path_alias\AliasManager $path_alias_manager */
    $path_alias_manager = \Drupal::service('path_alias.manager');
    $new_alias = $path_alias_manager->getAliasByPath(
      '/node/' . $created_node->id()
    );
    $old_alias = $node_data['node']['alias'];
    if (!str_starts_with($old_alias, '/')) {
      $old_alias = '/' . $old_alias;
    }
    if ($new_alias === $old_alias) {
      return TRUE;
    }
    /** @var \Drupal\path_alias\PathAliasStorage $path_storage */
    try {
      $path_storage = \Drupal::entityTypeManager()->getStorage('path_alias');
    } catch (InvalidPluginDefinitionException|PluginNotFoundException $e) {
      echo t(
          'Failed to get entity storage class. Exception: '
        ) . $e->getMessage() . "\n";
      return FALSE;
    }
    $add_alias['path'] = '/node/' . $created_node->id();
    $add_alias['alias'] = $old_alias;
    $add_alias['status'] = 1;
    /** @var \Drupal\path_alias\Entity\PathAlias $create_alias */
    $create_alias = $path_storage->create($add_alias);
    $create_alias->enforceIsNew();
    try {
      $create_alias->save();
      return $add_alias['alias'];
    } catch (EntityStorageException $e) {
      echo t(
          'Archport - import_node_alias - Failed to save old alias to new site. Exception: '
        ) . $e->getMessage() . t(' Imported node new id: ') . $created_node->id() . t(' Imported node old id: ') . $node_data['nid'] . "\n";
      return FALSE;
    }
  }

  public static function _archport_create_nid_alias(
    Node $created_node,
    array $node_data
  ) {
    // Add the old alias
    /** @var \Drupal\path_alias\AliasManager $path_alias_manager */
    $path_alias_manager = \Drupal::service('path_alias.manager');
    $new_alias = $path_alias_manager->getAliasByPath(
      '/node/' . $created_node->id()
    );
    $nid_alias = '/node/' . $node_data['node']['nid'];
    if ($new_alias === $nid_alias) {
      // Just in case the node id's ended up the same...
      return TRUE;
    }
    /** @var \Drupal\path_alias\PathAliasStorage $path_storage */
    try {
      $path_storage = \Drupal::entityTypeManager()->getStorage('path_alias');
    } catch (InvalidPluginDefinitionException|PluginNotFoundException $e) {
      echo t(
          'Failed to get entity storage class. Exception: '
        ) . $e->getMessage() . "\n";
      return FALSE;
    }
    $add_alias['path'] = '/node/' . $created_node->id();
    $add_alias['alias'] = $nid_alias;
    $add_alias['status'] = 1;
    $create_alias = $path_storage->create($add_alias);
    $create_alias->enforceIsNew();
    try {
      $create_alias->save();
      return $add_alias['alias'];
    } catch (EntityStorageException $e) {
      echo t(
          'Archport - create_nid_alias - Failed to save nid alias to new site. Exception: '
        ) . $e->getMessage() . t(' Imported node new id: ') . $created_node->id() . t(' Imported node old id: ') . $node_data['nid'] . "\n";
      return FALSE;
    }
  }

  public static function _archport_write_redirect_line(
    $archport_directory,
    $redirect_type,
    $settings,
    $create,
    $data
  ) {
    if (empty($settings['source_site_base_url'])) {
      echo t(
          'Archport - write_redirect_line - Setting "source_site_base_url" is not set. Aborting.'
        ) . "\n";
      return FALSE;
    }
    if (empty($settings['target_site_base_url'])) {
      echo t(
          'Archport - write_redirect_line - Setting "target_site_base_url" is not set. Aborting.'
        ) . "\n";
      return FALSE;
    }
    $source_base = rtrim($settings['source_site_base_url'], '/');
    $target_base = rtrim($settings['target_site_base_url'], '/');
    $source_node_path = 'node/' . $data['node']['nid'];
    $target_node_path = 'node/' . $create->id();
    $source_uri_node_id = $source_base . '/' . $source_node_path;
    $target_uri_node_id = $target_base . '/' . $target_node_path;
    if (!empty($data['node']['alias'])) {
      $source_alias_uri = $source_base . '/' . trim(
          $data['node']['alias'],
          '/'
        );
      $target_alias_uri = $target_base . '/' . trim(
          $data['node']['alias'],
          '/'
        );
    }
    if ($redirect_type == 'haproxy') {
      $file_out = $archport_directory . '/haproxy_redirects.txt';
      $source_haproxy_node_id_base = FileUtilities::_archport_get_uri_target(
        $source_uri_node_id
      );
      $redirect_node_id_line = "redirect location $target_uri_node_id code 301 if { base -i $source_haproxy_node_id_base }\n";
      if (!empty($data['node']['alias']) && !empty($target_alias_uri) && !empty($source_alias_uri)) {
        $source_haproxy_alias_base = FileUtilities::_archport_get_uri_target(
          $source_alias_uri
        );
        if (!empty($settings['create_aliases'])) {
          $redirect_alias_line = "redirect location $target_alias_uri code 301 if { base -i $source_haproxy_alias_base }\n";
        }
        else {
          $redirect_alias_line = "redirect location $target_uri_node_id code 301 if { base -i $source_haproxy_alias_base }\n";
        }
      }
    }
    elseif ($redirect_type == 'apache') {
      $file_out = $archport_directory . '/apache_redirects.txt';
      $redirect_node_id_line = "Redirect 301 /" . trim(
          $source_node_path,
          '/'
        ) . " $target_uri_node_id\n";
      if (!empty($data['node']['alias']) && !empty($target_alias_uri) && !empty($source_alias_uri)) {
        if (!empty($settings['create_aliases'])) {
          $redirect_alias_line = "Redirect 301 /" . $data['node']['alias'] . " $target_alias_uri\n";
        }
        else {
          $redirect_alias_line = "Redirect 301 /" . $data['node']['alias'] . " $target_uri_node_id\n";
        }
      }
    }
    else {
      echo t(
          'Archport - write_redirect_line - redirect_type is invalid. Aborting.'
        ) . "\n";
      return FALSE;
    }
    /** @var \Drupal\Core\File\FileSystem $fileSystem */
    $fileSystem = Drupal::service('file_system');
    $path = $fileSystem->realpath($file_out);
    $fh = fopen($path, 'a');
    if (empty($fh)) {
      echo t(
          'Archport - write_redirect_line - Failed to open redirect text file. Aborting.'
        ) . "\n";
      return FALSE;
    }
    $write_node_redirect_line = fwrite($fh, $redirect_node_id_line);
    if (!$write_node_redirect_line) {
      echo t(
          'Archport - write_redirect_line - Failed to write node id redirect line.'
        ) . "\n";
    }
    if (!empty($redirect_alias_line)) {
      $write_alias_redirect_line = fwrite($fh, $redirect_alias_line);
      if (!$write_alias_redirect_line) {
        echo t(
            'Archport - write_redirect_line - Failed to write alias redirect line.'
          ) . "\n";
      }
    }
    fclose($fh);
    if (empty($write_alias_redirect_line) || empty($write_node_redirect_line)) {
      return FALSE;
    }
    else {
      return TRUE;
    }
  }

}
