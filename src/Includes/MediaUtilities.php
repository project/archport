<?php

namespace Drupal\archport\Includes;

use Drupal;

class MediaUtilities {

  /**
   * Load a media entity by its id.
   *
   * @param int $id
   *
   * @return bool|\Drupal\media\Entity\Media
   */
  public static function _archport_load_media_by_target_id(int $id
  ): bool|Drupal\Core\Entity\EntityInterface {
    /** @var \Drupal\media\MediaStorage $mediaStorage */
    try {
      $mediaStorage = Drupal::entityTypeManager()->getStorage('media');
    } catch (Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException|Drupal\Component\Plugin\Exception\PluginNotFoundException $e) {
      echo t(
          'Failed to get entity storage class. Exception: '
        ) . $e->getMessage() . "\n";
      return FALSE;
    }
    /** @var \Drupal\media\Entity\Media $mediaLoad */
    $mediaLoad = $mediaStorage->load($id);
    if (!empty($mediaLoad)) {
      return $mediaLoad;
    }
    else {
      echo t(
          'Archport - load_media_by_target_id - Failed to load media for given id.'
        ) . "\n";
      return FALSE;
    }
  }

  /**
   * Process an array of Media items into Archport format.
   *
   * @param string $target_directory
   * @param array  $found_media
   *
   * @return bool|array
   */
  public static function _archport_process_exported_media(
    string $target_directory,
    array $found_media
  ): bool|array {
    if (empty($found_media)) {
      return TRUE;
    }
    /** @var \Drupal\file\FileStorage $fileStorage */
    try {
      $fileStorage = \Drupal::entityTypeManager()->getStorage('file');
    } catch (Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException|Drupal\Component\Plugin\Exception\PluginNotFoundException $e) {
      echo t(
          'Failed to get entity storage class. Exception: '
        ) . $e->getMessage() . "\n";
      return FALSE;
    }
    $processed_media = [];
    foreach ($found_media as $found_item) {
      $media_field_name = $found_item['field_name'];
      $target_id = $found_item['target_id'];
      /** @var \Drupal\media\Entity\Media $media */
      $media = $found_item['media'];
      $bundle = $media->bundle();
      $uid = $media->getOwnerId();
      $name = $media->getName();
      $created = $media->getCreatedTime();
      $changed = $media->getChangedTime();
      // Media entities have fields, so we need to find them.
      $fieldDefinitions = $media->getFieldDefinitions();
      $possible_fields = array_keys($fieldDefinitions);
      $found_fields = [];
      foreach ($possible_fields as $possible_field_name) {
        if (str_starts_with($possible_field_name, 'field_')) {
          // We only care about field_* fields.
          // Save the definition for later use.
          $found_fields[$possible_field_name]['definition'] = $fieldDefinitions[$possible_field_name];
          // Now get the actual field for this entity.
          $found_fields[$possible_field_name]['field'] = $media->get(
            $possible_field_name
          );
        }
      }
      $media_fields = [];
      foreach ($found_fields as $field_name => $field_info) {
        // Go through any found fields and process them.
        /** @var \Drupal\Core\Field\FieldItemList $field_list */
        $field_list = $field_info['field'];
        // The $media->get() call will have returned a FieldItemList, even
        // if there is only one item. So we use a for loop and the
        // FieldItemList::get() method to get the field's data.
        for ($f = 0; $f < $field_list->count(); $f++) {
          try {
            $field = $field_list->get($f);
          } catch (Drupal\Core\TypedData\Exception\MissingDataException $e) {
            echo t(
                'Archport - process_exported_media - Failed to get the media subfield. Aborting. Exception thrown: '
              ) . $e->getMessage() . "\n";
            return FALSE;
          }
          $field_value = $field->getValue();
          $file = $fileStorage->load($field_value['target_id']);
          // Process the file. This will copy it to the archport directory
          // and will return an array with the files' info in it.
          $process_file = FileUtilities::_archport_process_file(
            $target_directory,
            $file
          );
          if (!empty($process_file)) {
            // We want to save the data from $field_value,
            // so merge it with the $process_file array.
            $result = $field_value;
            foreach ($process_file as $pf_key => $pf_val) {
              $result[$pf_key] = $pf_val;
            }
            $media_fields[$field_name][] = $result;
          }
          else {
            echo t(
                'Archport - process_exported_media - Failed to process the file for a media subfield. Aborting.'
              ) . "\n";
            return FALSE;
          }
        }
      }
      // And we have finished with this item, so save it to the array we
      // will be returning.
      $processed_media[] = [
        'media_field_name' => $media_field_name,
        'target_id' => $target_id,
        'bundle' => $bundle,
        'uid' => $uid,
        'name' => $name,
        'created' => $created,
        'changed' => $changed,
        'sub_fields' => $media_fields,
      ];
    }
    return $processed_media;
  }

  /**
   * Import a media item.
   *
   * @param string $import_dir
   * @param array  $item
   * @param array  $map
   * @param array  $settings
   *
   * @return \Drupal\Core\Entity\EntityInterface|bool
   */
  public static function _archport_import_media_item(
    string $import_dir,
    array $item,
    array $map,
    array $settings
  ): Drupal\Core\Entity\EntityInterface|bool {
    try {
      /** @var \Drupal\media\MediaStorage $mediaStorage */
      $mediaStorage = Drupal::entityTypeManager()->getStorage('media');
    } catch (Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException|Drupal\Component\Plugin\Exception\PluginNotFoundException $e) {
      echo t(
          'Failed to get entity storage class. Exception: '
        ) . $e->getMessage() . "\n";
      return FALSE;
    }
    $media['bundle'] = $item['bundle'];
    if (!empty($map['users'][$item['uid']]['target_uid'])) {
      $media['uid'] = $map['users'][$item['uid']]['target_uid'];
    }
    else {
      $media['uid'] = 0;
    }
    $media['name'] = $item['name'];
    $media['created'] = $item['created'];
    $media['changed'] = $item['changed'];
    $imported_files = [];
    foreach ($item['sub_fields'] as $field_name => $field_value) {
      foreach ($field_value as $file_info) {
        $file = FileUtilities::_archport_import_file(
          $import_dir,
          $file_info,
          $settings
        );
        if (!$file) {
          return FALSE;
        }
        $new_file['target_id'] = $file->id();
        if (!empty($file_info['display'])) {
          $new_file['display'] = $file_info['display'];
        }
        if (!empty($file_info['description'])) {
          $new_file['description'] = $file_info['description'];
        }
        $new_file['uid'] = $file_info['uid'];
        $new_file['filename'] = $file_info['filename'];
        $new_file['filemime'] = $file_info['filemime'];
        $new_file['created'] = $file_info['created'];
        $new_file['changed'] = $file_info['changed'];
        $imported_files[$field_name][] = $new_file;
      }
    }
    foreach ($imported_files as $field_name => $files) {
      $media[$field_name] = $files;
    }
    $create = $mediaStorage->create($media);
    $create->enforceIsNew();
    try {
      $create->save();
      $load = $mediaStorage->load($create->id());
      if (empty($load)) {
        echo t(
            'Archport - import_media_item - Failed to load new media item.'
          ) . "\n";
        return FALSE;
      }
      else {
        return $load;
      }
    } catch (Drupal\Core\Entity\EntityStorageException $e) {
      echo t(
          'Archport - import_media_item - Failed to save new media item. Exception: '
        ) . $e->getMessage() . "\n";
      return FALSE;
    }
  }

}
