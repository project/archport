<?php
/** @file Archport functions dealing with taxonomy vocabularies and terms. */

namespace Drupal\archport\Includes;

use Drupal;
use JetBrains\PhpStorm\ArrayShape;

class TaxonomyUtilities {

  /**
   * Load a term by its tid and turn it into a custom array.
   *
   * @param int $tid
   *
   * @return array|bool
   */
  #[ArrayShape([
    'tid' => "",
    'name' => "mixed",
    'description' => "string",
    'weight' => "int",
    'vocabulary' => "array",
  ])] public static function _archport_load_term(int $tid): array|bool {
    try {
      /** @var \Drupal\taxonomy\TermStorage $termStorage */
      $termStorage = Drupal::entityTypeManager()
        ->getStorage('taxonomy_term');
    } catch (Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException|Drupal\Component\Plugin\Exception\PluginNotFoundException $e) {
      echo t(
          'Failed to get entity storage class. Exception: '
        ) . $e->getMessage() . "\n";
      return FALSE;
    }
    /** @var \Drupal\taxonomy\Entity\Term $termLoad */
    $termLoad = $termStorage->load($tid);
    try {
      /** @var \Drupal\taxonomy\VocabularyStorage $vocabularyStorage */
      $vocabularyStorage = Drupal::entityTypeManager()
        ->getStorage('taxonomy_vocabulary');
    } catch (Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException|Drupal\Component\Plugin\Exception\PluginNotFoundException $e) {
      echo t(
          'Failed to get entity storage class. Exception: '
        ) . $e->getMessage() . "\n";
      return FALSE;
    }
    /** @var \Drupal\taxonomy\Entity\Vocabulary $vocabularyLoad */
    $vocabularyLoad = $vocabularyStorage->load($termLoad->bundle());
    return [
      'tid' => $tid,
      'name' => $termLoad->getName(),
      'description' => $termLoad->getDescription(),
      'weight' => $termLoad->getWeight(),
      'vocabulary' => [
        'vid' => $termLoad->bundle(),
        'name' => $vocabularyLoad->get('name'),
        'machine_name' => $termLoad->bundle(),
        'description' => $vocabularyLoad->get('description'),
        'hierarchy' => $vocabularyLoad->get('hierarchy'),
        'weight' => $vocabularyLoad->get('weight'),
      ],
    ];
  }

  /**
   * Find vocabularies and terms from an exported nodes' data in $data.
   * Return them as an array.
   *
   * @param array $data
   *
   * @return array[]
   */
  #[ArrayShape([
    'vocabularies' => "array",
    'terms' => "array",
  ])] public static function _archport_get_node_vocabularies_and_terms_from_data(
    array $data
  ): array {
    $vocabularies = [];
    $terms = [];
    if (!empty($data['taxonomies'])) {
      foreach ($data['taxonomies'] as $vid => $vocabulary) {
        $vocabularies[$vid] = [
          'name' => $vocabulary['name'],
          'machine_name' => $vocabulary['machine_name'],
          'hierarchy' => $vocabulary['hierarchy'],
          'description' => $vocabulary['description'],
        ];
        foreach ($vocabulary['terms'] as $term) {
          $terms[$term['tid']] = [
            'vocabulary' => $vocabulary['name'],
            'description' => $term['description'],
            'field_name' => $term['field_name'],
            'name' => $term['name'],
            'tid' => $term['tid'],
            'weight' => $term['weight'],
          ];
        }
      }
    } // else just return the empty arrays.
    return ['vocabularies' => $vocabularies, 'terms' => $terms];
  }

  /**
   * Import vocabularies from map.json. Unless the settings say otherwise.
   *
   * @param array $map
   * @param array $settings
   *
   * @return bool
   */
  public static function _archport_import_vocabularies(
    array $map,
    array $settings
  ): bool {
    if ($settings['skip_vocabularies']) {
      return TRUE;
    }
    if (!$settings['create_vocabularies']) {
      return TRUE;
    }
    if (empty($map['vocabularies'])) {
      return TRUE;
    }
    try {
      $vocabularyStorage = Drupal::entityTypeManager()->getStorage(
        'taxonomy_vocabulary'
      );
    } catch (Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException|Drupal\Component\Plugin\Exception\PluginNotFoundException $e) {
      echo t(
          'Failed to get entity storage class. Exception: '
        ) . $e->getMessage() . "\n";
      return FALSE;
    }
    $errors = FALSE;
    foreach ($map['vocabularies'] as $vocabulary) {
      // Sometimes data is bad.
      if (empty($vocabulary) || empty($vocabulary['source_machine_name'])) {
        continue;
      }
      $load_vocabulary = $vocabularyStorage->loadByProperties(
        ['vid' => $vocabulary['source_machine_name']]
      );
      if (!empty($load_vocabulary)) {
        continue;
      }
      $newVocabulary = [];
      if (!empty($vocabulary['target_name'])) {
        $newVocabulary['name'] = $vocabulary['target_name'];
      }
      else {
        $newVocabulary['name'] = $vocabulary['source_name'];
      }
      if (!empty($vocabulary['target_machine_name'])) {
        $newVocabulary['vid'] = $vocabulary['target_machine_name'];
      }
      else {
        $newVocabulary['vid'] = $vocabulary['source_machine_name'];
      }
      if (!empty($vocabulary['target_description'])) {
        $newVocabulary['description'] = $vocabulary['target_description'];
      }
      else {
        $newVocabulary['description'] = $vocabulary['source_description'];
      }

      // Check if the vocabulary already exists.
      $load = $vocabularyStorage->load($newVocabulary['vid']);
      if ($load === NULL) {
        // Vocabulary did not exist, so try to create it.
        $create = $vocabularyStorage->create($newVocabulary);
        $create->enforceIsNew();
        try {
          $create->save();
          echo t(
              "Archport - import_vocabularies - Saved new vocabulary with id: "
            )
            . $newVocabulary['vid'] . "\n";
        } catch (Drupal\Core\Entity\EntityStorageException $e) {
          $errors = TRUE;
          echo t(
              "Archport - import_vocabularies - Failed to save new vocabulary with id: "
            )
            . $newVocabulary['vid'] . t(' Exception: ') . $e->getMessage() . "\n";
        }
      }
    }
    if ($errors) {
      return FALSE;
    }
    else {
      return TRUE;
    }
  }

  /**
   * Import terms from map.json. Unless the settings say otherwise.
   *
   * @param array $map
   * @param array $settings
   *
   * @return bool
   */
  public static function _archport_import_terms(
    array $map,
    array $settings
  ): bool {
    if ($settings['skip_terms']) {
      return TRUE;
    }
    if (!$settings['create_terms']) {
      return TRUE;
    }
    if (empty($map['terms'])) {
      return TRUE;
    }
    try {
      $termStorage = Drupal::entityTypeManager()->getStorage('taxonomy_term');
    } catch (Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException|Drupal\Component\Plugin\Exception\PluginNotFoundException $e) {
      echo t(
          'Failed to get entity storage class. Exception: '
        ) . $e->getMessage() . "\n";
      return FALSE;
    }
    $errors = FALSE;
    foreach ($map['terms'] as $term) {
      // Sometimes data is bad.
      if (empty($term) || empty($term['source_vocabulary']) || empty($term['source_name'])) {
        continue;
      }
      $load_term = $termStorage->loadByProperties(
        ['vid' => $term['source_vocabulary'], 'name' => $term['source_name']]
      );
      if (!empty($load_term)) {
        continue;
      }
      $source_vocabulary = $term['source_vocabulary'];
      if (!empty($map['vocabularies'][$source_vocabulary]['target_machine_name'])) {
        $target_vocabulary_machine_name = $map['vocabularies'][$source_vocabulary]['target_machine_name'];
      }
      else {
        $target_vocabulary_machine_name = $map['vocabularies'][$source_vocabulary]['source_machine_name'];
      }
      $newTerm = [];
      if (!empty($term['target_name'])) {
        $newTerm['name'] = $term['target_name'];
      }
      else {
        $newTerm['name'] = $term['source_name'];
      }
      if (!empty($term['target_description'])) {
        $newTerm['description'] = $term['target_description'];
      }
      else {
        $newTerm['description'] = $term['source_description'];
      }
      if (!empty($term['target_weight'])) {
        $newTerm['weight'] = $term['target_weight'];
      }
      else {
        $newTerm['weight'] = $term['source_weight'];
      }
      $newTerm['vid'] = $target_vocabulary_machine_name;

      // Does the term already exist?
      $load = $termStorage->loadByProperties([
        'vid' => $newTerm['vid'],
        'name' => $newTerm['name'],
      ]);
      // If the count of $load is not 0, the term already exists.
      if (count($load) == 0) {
        $create = $termStorage->create($newTerm);
        $create->enforceIsNew();
        try {
          $create->save();
          echo t(
              "Archport - import_terms - Saved new term with name: "
            ) . $newTerm['name'] . "\n";
        } catch (Drupal\Core\Entity\EntityStorageException $e) {
          $errors = TRUE;
          echo t(
              "Archport - import_terms - Failed to save new term with name: "
            ) . $term['target_name'] . t(' Exception: ') . $e->getMessage() . "\n";
        }
      }// no need for else.
    }
    if ($errors) {
      return FALSE;
    }
    else {
      return TRUE;
    }
  }

}
