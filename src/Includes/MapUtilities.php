<?php
/** @file
 *      Archport functions related to the map.json file.
 */

namespace Drupal\archport\Includes;

use Drupal;

class MapUtilities {

  /**
   * Write a map.json file based on a single node's exported data.
   * Optionally override existing an existing map.json.
   *
   * @param string $archport_directory The main Archport directory.
   * @param array $node_data The array of node data from a data.json
   *                                   file
   *
   * @return bool|array
   */
  public static function _archport_write_single_node_map(
    string $archport_directory,
    array $node_data
  ): bool|array {
    $map = [];
    if (file_exists($archport_directory)) {
      $map_uri = $archport_directory . '/map.json';
      $map = MapUtilities::_archport_read_map($archport_directory);
      if (!$map) {
        echo t(
            "Archport - write_single_node_map - Map file did not exist and could not be created. Aborting."
          ) . "\n";
        return FALSE;
      }

      // First gather the data we need to map from the exported data.
      $users = NodeUtilities::_archport_get_node_users_from_data($node_data);
      $content_type = NodeUtilities::_archport_get_node_content_type_from_data(
        $node_data
      );
      $vocabularies_and_terms = TaxonomyUtilities::_archport_get_node_vocabularies_and_terms_from_data(
        $node_data
      );
      $vocabularies = $vocabularies_and_terms['vocabularies'];
      $terms = $vocabularies_and_terms['terms'];
      $fields = NodeUtilities::_archport_get_node_fields_from_data($node_data);

      // Now we set up the mapping structure.
      $map = MapUtilities::_archport_map_users($map, $users);
      $map = MapUtilities::_archport_map_content_types($map, [$content_type]);
      $map = MapUtilities::_archport_map_vocabularies($map, $vocabularies);
      $map = MapUtilities::_archport_map_terms($map, $terms);
      $map = MapUtilities::_archport_map_fields($map, $fields);

      return MapUtilities::_archport_write_map_file($map, $archport_directory);
    }
    else {
      echo
        t(
          'Archport - write_single_node_map - The root Archport destination directory does not exist. Please run the archport:initialize command.'
        ) . "\n";
      return FALSE;
    }
  }

  /**
   * If map.json exists, read it into an array and return. If it does not exist,
   * create the file and return an empty array.
   *
   * Return false if anything fails.
   *
   * @param string $archport_directory The main Archport directory.
   *
   * @return bool|array
   */
  public static function _archport_read_map(string $archport_directory): mixed {
    $map_uri = $archport_directory . '/map.json';
    /** @var \Drupal\Core\File\FileSystemInterface $fileSystem */
    $fileSystem = Drupal::service('file_system');
    $map = [];
    if (file_exists($map_uri)) {
      $mapJson = file_get_contents($map_uri);
      if ($mapJson != '[]') {
        // json_decode errors on an empty array like this, so
        // if it is an empty array, just use the already set $map.
        // Otherwise, try to decode it.
        $map = json_decode($mapJson, TRUE);
        if ($map === FALSE || $map === NULL) {
          echo t(
              "Archport - read_map - The map.json file contains invalid json."
            ) . "\n";
          return FALSE;
        }
      }// No need for else.
    }
    else {
      echo t(
          "Archport - read_map - The map.json file did not exist so we will create it."
        ) . "\n";
      $create = $fileSystem->saveData(
        json_encode($map, TRUE),
        $map_uri
      );
      if (!$create) {
        echo t(
            "Archport - read_map - Failed to create the map.json file."
          ) . "\n";
        return FALSE;
      }// Else the map is empty, so just use the already existing $map.
    }
    return $map;
  }

  /**
   * Take one or more user arrays and map them into map.json
   *
   * @param array $map
   * @param array $users
   *
   * @return array
   */
  public static function _archport_map_users(array $map, array $users): array {
    if (!isset($map['users'])) {
      $map['users'] = [];
    }
    if (!empty($users)) {
      if (isset($users['uid'])) {
        $map = MapUtilities::_archport_map_users_process_data($map, $users);
      }
      else {
        // We should have more than one user.
        foreach ($users as $user) {
          $map = MapUtilities::_archport_map_users_process_data($map, $user);
        }
      }
    }
    return $map;
  }

  /**
   * Add a given user to the map's users section.
   * This public static function checks to make sure existing data is not
   * overwritten.
   *
   * @param array $map
   * @param array $users
   *
   * @return array
   */
  public static function _archport_map_users_process_data(
    array $map,
    array $users
  ): array {
    if (!isset($map['users'][$users['uid']]['source_created'])) {
      $map['users'][$users['uid']]['source_created'] = $users['created'];
    }
    if (!isset($map['users'][$users['uid']]['target_created'])) {
      $map['users'][$users['uid']]['target_created'] = '';
    }
    if (!isset($map['users'][$users['uid']]['source_email'])) {
      $map['users'][$users['uid']]['source_email'] = $users['email'];
    }
    if (!isset($map['users'][$users['uid']]['target_email'])) {
      $map['users'][$users['uid']]['target_email'] = '';
    }
    if (!isset($map['users'][$users['uid']]['source_uid'])) {
      $map['users'][$users['uid']]['source_uid'] = $users['uid'];
    }
    if (!isset($map['users'][$users['uid']]['target_uid'])) {
      $map['users'][$users['uid']]['target_uid'] = '';
    }
    if (!isset($map['users'][$users['uid']]['source_updated'])) {
      $map['users'][$users['uid']]['source_updated'] = $users['updated'];
    }
    if (!isset($map['users'][$users['uid']]['target_updated'])) {
      $map['users'][$users['uid']]['target_updated'] = '';
    }
    if (!isset($map['users'][$users['uid']]['source_username'])) {
      $map['users'][$users['uid']]['source_username'] = $users['username'];
    }
    if (!isset($map['users'][$users['uid']]['target_username'])) {
      $map['users'][$users['uid']]['target_username'] = '';
    }
    return $map;
  }

  /**
   * Add the content machine names in $content_types to the content_types
   * field in $map.
   * This public static function checks to make sure existing data is not
   * overwritten.
   *
   * @param array $map
   * @param array $content_types
   *
   * @return mixed
   */
  public static function _archport_map_content_types(
    array $map,
    array $content_types
  ): mixed {
    // Map content types as < source > => < target >.
    if (!isset($map['content_types'])) {
      $map['content_types'] = [];
    }
    if (is_array($content_types)) {
      foreach ($content_types as $content_type) {
        if (!isset($map['content_types'][$content_type])) {
          $map['content_types'][$content_type] = '';
        }
      }
    }
    else {
      if (!isset($map['content_types'][$content_types])) {
        $map['content_types'][$content_types] = '';
      }
    }
    return $map;
  }

  /**
   * Add the vocabularies in $vocabularies to the map in $map.
   * This public static function checks to make sure existing data is not
   * overwritten.
   *
   * @param array $map
   * @param array $vocabularies
   *
   * @return mixed
   */
  public static function _archport_map_vocabularies(
    array $map,
    array $vocabularies
  ): mixed {
    // Map vocabularies with a sub array.
    if (!isset($map['vocabularies'])) {
      $map['vocabularies'] = [];
    }
    $map_vocabularies = $map['vocabularies'];
    $data_vocabularies = $vocabularies;

    foreach ($data_vocabularies as $data_vocabulary) {
      if (!isset($map_vocabularies[$data_vocabulary['name']])) {
        $map_vocabularies[$data_vocabulary['name']] = [
          'source_name' => $data_vocabulary['name'],
          'target_name' => '',
          'source_machine_name' => $data_vocabulary['machine_name'],
          'target_machine_name' => '',
          'source_hierarchy' => $data_vocabulary['hierarchy'],
          'target_hierarchy' => '',
          'source_description' => $data_vocabulary['description'],
          'target_description' => '',
        ];
      }
      else {
        // Only update map if the map hasn't already been set up.
        if (!isset($map_vocabularies[$data_vocabulary['name']]['source_name'])) {
          $map_vocabularies[$data_vocabulary['name']]['source_name'] = $data_vocabulary['name'];
        }
        if (!isset($map_vocabularies[$data_vocabulary['name']]['target_name'])) {
          $map_vocabularies[$data_vocabulary['name']]['target_name'] = '';
        }
        if (!isset($map_vocabularies[$data_vocabulary['name']]['source_machine_name'])) {
          $map_vocabularies[$data_vocabulary['name']]['source_machine_name'] = $data_vocabulary['machine_name'];
        }
        if (!isset($map_vocabularies[$data_vocabulary['name']]['target_machine_name'])) {
          $map_vocabularies[$data_vocabulary['name']]['target_machine_name'] = '';
        }
        if (!isset($map_vocabularies[$data_vocabulary['name']]['source_hierarchy'])) {
          $map_vocabularies[$data_vocabulary['name']]['source_hierarchy'] = $data_vocabulary['hierarchy'];
        }
        if (!isset($map_vocabularies[$data_vocabulary['name']]['target_hierarchy'])) {
          $map_vocabularies[$data_vocabulary['name']]['target_hierarchy'] = '';
        }
        if (!isset($map_vocabularies[$data_vocabulary['name']]['source_description'])) {
          $map_vocabularies[$data_vocabulary['name']]['source_description'] = $data_vocabulary['description'];
        }
        if (!isset($map_vocabularies[$data_vocabulary['name']]['target_description'])) {
          $map_vocabularies[$data_vocabulary['name']]['target_description'] = '';
        }
      }
    }
    $map['vocabularies'] = $map_vocabularies;
    return $map;
  }

  /**
   * Add the terms in $terms to the terms' field in the map $map.
   * This public static function checks to make sure existing data is not
   * overwritten.
   *
   * @param array $map
   * @param array $terms
   *
   * @return array
   */
  public static function _archport_map_terms(array $map, array $terms): array {
    if (empty($map['terms'])) {
      $map['terms'] = [];
    }
    // Map terms with a sub array.
    foreach ($terms as $term) {
      // Check if the term already exists.
      $exists = FALSE;
      foreach ($map['terms'] as $key => $value) {
        if ($value['source_name'] === $term['name'] && $value['source_vocabulary'] === $term['vocabulary']) {
          // It already exists.
          $exists = TRUE;
          // Only update map if the map hasn't already been set up.
          if (empty($map['terms'][$key]['source_vocabulary'])) {
            $map['terms'][$key]['source_vocabulary'] = $term['vocabulary'];
          }
          if (empty($map['terms'][$key]['target_vocabulary'])) {
            $map['terms'][$key]['target_vocabulary'] = '';
          }
          if (empty($map['terms'][$key]['source_description'])) {
            $map['terms'][$key]['source_description'] = $term['description'];
          }
          if (empty($map['terms'][$key]['target_description'])) {
            $map['terms'][$key]['target_description'] = '';
          }
          if (empty($map['terms'][$key]['source_name'])) {
            $map['terms'][$key]['source_name'] = $term['name'];
          }
          if (empty($map['terms'][$key]['target_name'])) {
            $map['terms'][$key]['target_name'] = '';
          }
          if (empty($map['terms'][$key]['source_weight'])) {
            $map['terms'][$key]['source_weight'] = $term['weight'];
          }
          if (empty($map['terms'][$key]['target_weight'])) {
            $map['terms'][$key]['target_weight'] = '';
          }
          break;
        }
      }
      if (empty($exists)) {
        $map['terms'][] = [
          'source_vocabulary' => $term['vocabulary'],
          'target_vocabulary' => '',
          'source_description' => $term['description'],
          'target_description' => '',
          'source_name' => $term['name'],
          'target_name' => '',
          'source_weight' => $term['weight'],
          'target_weight' => '',
        ];
      }
    }
    return $map;
  }

  /**
   * Add the fields in $fields to the field list in the map $map.
   * This public static function checks to make sure existing data is not
   * overwritten.
   *
   * @param array $map
   * @param array $fields
   *
   * @return array
   */
  public static function _archport_map_fields(
    array $map,
    array $fields
  ): mixed {
    if (!isset($map['fields'])) {
      $map['fields'] = [];
    }
    // Map fields as < source field name > => < target field name >
    foreach ($fields as $field) {
      if (!isset($map['fields'][$field])) {
        $map['fields'][$field] = '';
      }
    }
    return $map;
  }

  /**
   * Write the data in $map as json to the path in $map_file_path.
   *
   * @param array $new_map_data
   * @param string $archport_directory
   *
   * @return false|string
   */
  public static function _archport_write_map_file(
    array $new_map_data,
    string $archport_directory
  ): bool|string {
    $map_uri = $archport_directory . '/map.json';
    // Read the current map file so that we don't overwrite anything.
    $current_map_data = MapUtilities::_archport_read_map($archport_directory);
    // Now merge the two arrays. array_merge_recursive did strange things,
    // so manually loop through each top level key.
    if (!empty($new_map_data['users'])) {
      foreach ($new_map_data['users'] as $key => $val) {
        if (isset($current_map_data['users'][$key])) {
          foreach ($new_map_data['users'][$key] as $field => $value) {
            if (!empty($value)) {
              // Override the old value with the new one.
              $current_map_data['users'][$key][$field] = $value;
            }
          }
        }
        else {
          $current_map_data['users'][$key] = $val;
        }
      }
    }
    if (!empty($new_map_data['content_types'])) {
      foreach ($new_map_data['content_types'] as $key => $val) {
        if (isset($current_map_data['content_types'][$key])) {
          continue;
        }
        else {
          $current_map_data['content_types'][$key] = $val;
        }
      }
    }
    if (!empty($new_map_data['vocabularies'])) {
      foreach ($new_map_data['vocabularies'] as $key => $val) {
        if (isset($current_map_data['vocabularies'][$key])) {
          continue;
        }
        else {
          $current_map_data['vocabularies'][$key] = $val;
        }
      }
    }
    if (!empty($new_map_data['terms'])) {
      foreach ($new_map_data['terms'] as $key => $val) {
        if (isset($current_map_data['terms'][$key])) {
          continue;
        }
        else {
          $current_map_data['terms'][$key] = $val;
        }
      }
    }
    if (!empty($new_map_data['fields'])) {
      foreach ($new_map_data['fields'] as $key => $val) {
        if (isset($current_map_data['fields'][$key])) {
          continue;
        }
        else {
          $current_map_data['fields'][$key] = $val;
        }
      }
    }
    if (!empty($new_map_data['field_types'])) {
      foreach ($new_map_data['field_types'] as $key => $val) {
        if (isset($current_map_data['field_types'][$key])) {
          continue;
        }
        else {
          $current_map_data['field_types'][$key] = $val;
        }
      }
    }

    /** @var \Drupal\Core\File\FileSystemInterface $fileSystem */
    $fileSystem = Drupal::service('file_system');
    // Now we write the actual file.
    $map_json = json_encode($current_map_data, JSON_PRETTY_PRINT);
    $save = $fileSystem->saveData(
      $map_json,
      $map_uri,
      Drupal\Core\File\FileSystemInterface::EXISTS_REPLACE
    );

    if (!$save) {
      echo t("Archport - write_map_file - Failed to save map.json.") . "\n";
      return FALSE;
    }
    else {
      echo t("Archport - write_map_file - Saved map.json to: ") . $save . "\n";
      return $save;
    }
  }

}
