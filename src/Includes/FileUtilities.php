<?php
/**
 * @file
 *      Archport functions that deal with exporting, importing, and otherwise,
 *      files.
 */

namespace Drupal\archport\Includes;

use Drupal;

class FileUtilities {

  /**
   * Process files exported from a node.
   *
   * @param string $export_node_to_directory The subdirectory that the nodes'
   *                                         data.json file will be saved to.
   * @param array  $files                    An array of files.
   *
   * @return array|false
   */
  public static function _archport_process_exported_files(
    string $export_node_to_directory,
    array $files
  ): bool|array {
    foreach ($files as $key => $file_data) {
      /** @var \Drupal\file\Entity\File $file */
      $file = $file_data['file'];

      $process_file = FileUtilities::_archport_process_file(
        $export_node_to_directory,
        $file
      );
      if (!$process_file) {
        return FALSE;
      }
      else {
        // Add the process results to the files array.
        foreach ($process_file as $sub_key => $val) {
          $files[$key][$sub_key] = $val;
        }
        // Can't store the object, so unset it.
        unset($files[$key]['file']);
      }
    }
    return $files;
  }

  /**
   * Get the file info for data.json, and the copy the file to the correct
   * destination directory.
   *
   * @param string                   $export_node_to_directory The directory
   *                                                           the parent node
   *                                                           is being
   *                                                           exported to.
   * @param \Drupal\file\Entity\File $file                     The file entity
   *                                                           being exported.
   *
   * @return bool|array
   */
  public static function _archport_process_file(
    string $export_node_to_directory,
    Drupal\file\Entity\File $file
  ): bool|array {
    /** @var Drupal\Core\File\FileSystem $fileSystem */
    $fileSystem = Drupal::service('file_system');
    // Create a directory where we can store the files.
    $file_copy_path_public = rtrim(
        $export_node_to_directory,
        '/'
      ) . '/files/public';
    $file_copy_path_private = rtrim(
        $export_node_to_directory,
        '/'
      ) . '/files/private';
    $file_copy_path_unknown = rtrim(
        $export_node_to_directory,
        '/'
      ) . '/files/unknown';
    $uri = $file->getFileUri();
    $scheme = FileUtilities::_archport_get_uri_scheme($uri);
    $sub_directory = rtrim(
      ltrim(FileUtilities::_archport_get_uri_target($uri), '/'),
      '/'
    );
    if ($scheme === 'public') {
      $file_destination = $file_copy_path_public . '/' . $sub_directory;
    }
    elseif ($scheme === 'private') {
      $file_destination = $file_copy_path_private . '/' . $sub_directory;
    }
    else {
      $file_destination = $file_copy_path_unknown . '/' . $sub_directory;
    }

    // Make sure the correct subdirectory exists
    $real_folder = Utilities::_archport_prepare_directory(
      $fileSystem->dirname($file_destination)
    );
    if (empty($real_folder)) {
      echo t(
          'Archport - process_file - Failed to prepare file destination directory. Aborting.'
        ) . "\n";
      return FALSE;
    }
    // Copy the file.
    $copy_file = $fileSystem->copy(
      $uri,
      $file_destination,
      Drupal\Core\File\FileSystemInterface::EXISTS_REPLACE
    );
    if (!$copy_file) {
      echo t(
          "Archport - process_file - Failed to copy file to archport destination directory."
        ) . "\n";

      return FALSE;
    }
    else {
      $copied_relative_path = trim(
        str_replace($export_node_to_directory, '', $file_destination),
        '/'
      );
      // Return an array with the needed file info
      $result = [];
      $result['uid'] = $file->getOwnerId();
      $result['uri'] = $uri;
      $result['filename'] = $file->getFilename();
      $result['filemime'] = $file->getMimeType();
      $result['created'] = $file->getCreatedTime();
      $result['changed'] = $file->getChangedTime();
      $result['archport_path'] = $copied_relative_path;
      return $result;
    }
  }

  /**
   * Returns the scheme of an uri. Given 'public://target/dir' it will return
   * 'public'.
   *
   * @param string $uri
   *
   * @return string
   */
  public static function _archport_get_uri_scheme(string $uri): string {
    $explode = explode('://', $uri);
    return $explode[0];
  }

  /**
   * Returns the target of an uri. Given'public://target/dir' it will return
   * 'target/dir'.
   *
   * @param string $uri
   *
   * @return string
   */
  public static function _archport_get_uri_target(string $uri): string {
    $explode = explode('://', $uri);
    return $explode[1];
  }

  /**
   * Load a Drupal file entity by its id.
   *
   * @param int $id
   *
   * @return bool|\Drupal\file\Entity\File
   */
  public static function _archport_load_file_by_target_id(int $id
  ): bool|Drupal\file\Entity\File {
    /** @var \Drupal\file\FileStorage $fileStorage */
    try {
      $fileStorage = \Drupal::entityTypeManager()->getStorage('file');
    } catch (Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException|Drupal\Component\Plugin\Exception\PluginNotFoundException $e) {
      echo t(
          'Failed to get entity storage class. Exception: '
        ) . $e->getMessage() . "\n";
      return FALSE;
    }
    /** @var \Drupal\file\Entity\File $fileLoad */
    $fileLoad = $fileStorage->load($id);
    if (!empty($fileLoad)) {
      return $fileLoad;
    }
    else {
      echo t(
          'Archport - load_file_by_target_id - Failed to load file for given id.'
        ) . "\n";
      return FALSE;
    }
  }

  /**
   * Import a single file.
   *
   * @param string $source_dir The directory the file being imported is stored
   *                           in.
   * @param array  $file_data  An array of information about the file. Likely
   *                           from a data.json file.
   * @param array  $settings   The settings array from settings.json.
   *
   * @return bool|\Drupal\file\Entity\File
   */
  public static function _archport_import_file(
    string $source_dir,
    array $file_data,
    array $settings
  ) {
    // First, sanity check that the source file actually exists.
    $source_path = $source_dir . '/' . $file_data['archport_path'];
    if (file_exists($source_path)) {
      /** @var Drupal\Core\File\FileSystem $fileSystem */
      $fileSystem = Drupal::service('file_system');
      // Figure out what directory the file should be copied into.
      // Override the files old path if that's been configured in settings.json
      $public_path = '';
      $existing_action = 'rename';
      if (!empty($settings['override_file_paths']['public']['override'])) {
        $public_path = $settings['override_file_paths']['public']['path'];
        $existing_action = $settings['override_file_paths']['public']['existing_action'];
      }
      $private_path = '';
      if (!empty($settings['override_file_paths']['private']['override'])) {
        $private_path = $settings['override_file_paths']['private']['path'];
        $existing_action = $settings['override_file_paths']['private']['existing_action'];
      }
      $unknown_path = '';
      if (!empty($settings['override_file_paths']['unknown']['override'])) {
        $unknown_path = $settings['override_file_paths']['unknown']['path'];
        $existing_action = $settings['override_file_paths']['unknown']['existing_action'];
      }
      // Now pick the correct path for the file based on its existing uri.
      if (!empty($public_path) && FileUtilities::_archport_get_uri_scheme(
          $file_data['uri']
        ) === 'public') {
        $target_directory = $public_path;
      }
      elseif (!empty($private_path) && FileUtilities::_archport_get_uri_scheme(
          $file_data['uri']
        ) === 'private') {
        $target_directory = $private_path;
      }
      elseif (!empty($unknown_path) && FileUtilities::_archport_get_uri_scheme(
          $file_data['uri']
        ) !== 'public' && FileUtilities::_archport_get_uri_scheme(
          $file_data['uri']
        ) !== 'private') {
        $target_directory = $unknown_path;
      }
      else {
        // We aren't overriding, so just use the directory path from the
        // old uri.
        $uri_target = FileUtilities::_archport_get_uri_target(
          $file_data['uri']
        );
        $uri_scheme = FileUtilities::_archport_get_uri_scheme(
          $file_data['uri']
        );
        $explode = explode('/', $uri_target);
        $sub = '';
        for ($i = 0; $i < count($explode) - 1; $i++) {
          $sub .= '/' . $explode[$i];
        }
        $sub = rtrim(ltrim($sub, '/'), '/');
        $target_directory = $uri_scheme . '://' . $sub;
      }

      // Make sure the target directory exists.
      if (!file_exists($target_directory)) {
        $prepare = $fileSystem->mkdir($target_directory, 0770, TRUE);
        if (!$prepare) {
          echo t(
              "Archport - archport_import_file - Failed to prepare target directory: "
            ) . $target_directory . "\n";
          return FALSE;
        }
      }

      // Set the full uri to the final destination.
      $new_file_uri = $target_directory . '/' . $file_data['filename'];
      if (file_exists($new_file_uri) && $existing_action === 'reuse') {
        // Somehow, the file already exists. So load its info from the database.
        return FileUtilities::_archport_load_file_object_by_uri($new_file_uri);
      }
      elseif (file_exists($new_file_uri) && $existing_action === 'replace') {
        return FileUtilities::_archport_copy_create_imported_file(
          $source_path,
          $target_directory,
          $new_file_uri,
          $file_data,
          $existing_action
        );
      }
      else {
        // Actually copy the file to its destination and make it managed.
        return FileUtilities::_archport_copy_create_imported_file(
          $source_path,
          $target_directory,
          $new_file_uri,
          $file_data,
          $existing_action
        );
      }
    }
    else {
      echo t(
          "Archport - archport_import_file - Failed to process file. Source file did not exist. "
        ) . $source_path;
      return FALSE;
    }
  }

  /**
   * Load the Drupal file entity based on a given uri.
   *
   * @param string $uri
   *
   * @return false|\Drupal\file\Entity\File
   */
  public static function _archport_load_file_object_by_uri(string $uri
  ): bool|Drupal\file\Entity\File {
    if (!file_exists($uri)) {
      echo t(
          "Archport - load_file_object - File did not exist. Uri: "
        ) . $uri;
      return FALSE;
    }
    /** @var \Drupal\file\FileStorage $fileStorage */
    try {
      $fileStorage = \Drupal::entityTypeManager()->getStorage('file');
    } catch (Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException|Drupal\Component\Plugin\Exception\PluginNotFoundException $e) {
      echo t(
          'Failed to get entity storage class. Exception: '
        ) . $e->getMessage() . "\n";
      return FALSE;
    }
    $fileLoad = $fileStorage->loadByProperties(['uri' => $uri]);

    if (count($fileLoad) === 1) {
      /** @var \Drupal\file\Entity\File $fileEntity */
      $fileEntity = array_pop($fileLoad);
      return $fileEntity;
    }
    elseif (count($fileLoad) > 1) {
      echo t(
          "Archport - load_file_object - Multiple files found for the same uri. You should fix this manually. Uri: "
        ) . $uri . "\n";
      return FALSE;
    }
    else {
      echo t(
          "Archport - load_file_object - No files found for this uri. You should remove any files in the following uri's directory for the node you are importing. Uri: "
        ) . $uri . "\n";
      return FALSE;
    }
  }

  /**
   * When importing a file, copy it to its appropriate destination, then
   * create a Drupal file object for it, and then save that to the target
   * database.
   *
   * @param string $source_path      Where the file is stored.
   * @param string $target_directory The directory where the file will be
   *                                 saved.
   * @param string $new_file_uri     Where the file should be imported to.
   * @param array  $file_data        An array of data about the file.
   * @param string $existing_action  What to do if the file exists in the
   *                                 target directory.
   *
   */
  public static function _archport_copy_create_imported_file(
    string $source_path,
    string $target_directory,
    string $new_file_uri,
    array $file_data,
    string $existing_action = 'rename'
  ): bool|Drupal\file\Entity\File {
    /** @var Drupal\Core\File\FileSystem $fileSystem */
    $fileSystem = Drupal::service('file_system');
    /** @var \Drupal\file\FileStorage $fileStorage */
    try {
      $fileStorage = \Drupal::entityTypeManager()->getStorage('file');
    } catch (Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException|Drupal\Component\Plugin\Exception\PluginNotFoundException $e) {
      echo t(
          'Failed to get entity storage class. Exception: '
        ) . $e->getMessage() . "\n";
      return FALSE;
    }
    if ($existing_action === 'rename') {
      $file_exists_action = Drupal\Core\File\FileSystemInterface::EXISTS_RENAME;
    }
    elseif ($existing_action === 'replace') {
      $file_exists_action = Drupal\Core\File\FileSystemInterface::EXISTS_REPLACE;
    }
    else {
      echo t(
          'Archport - copy_create_imported_file - Invalid \$existing_action value set. Aborting.'
        ) . "\n";
      return FALSE;
    }
    // Make sure the correct subdirectory exists
    $real_folder = $fileSystem->dirname($new_file_uri);
    if (!file_exists($real_folder)) {
      Utilities::_archport_prepare_directory($real_folder);
    }
    // Copy the file to its destination. Replace any existing files.
    $copy_file = $fileSystem->copy(
      $source_path,
      $new_file_uri,
      $file_exists_action
    );
    if (!$copy_file) {
      echo t(
          "Archport - archport_import_file - Failed to copy source file to target destination: "
        ) . $target_directory . '/' . $file_data['filename'];
      return FALSE;
    }
    // Create a new object for the file.
    $file = [];
    // Figure out what uid should own the file.
    if (!empty($map['users'][$file_data['uid']]['target_uid'])) {
      $target_uid = $map['users'][$file_data['uid']]['target_uid'];
    }
    else {
      $target_uid = 0;
    }
    // Set the needed properties on the new object.
    $file['uid'] = $target_uid;
    $file['filename'] = $file_data['filename'];
    $file['uri'] = $copy_file;
    $file['filemime'] = $file_data['filemime'];
    if (!empty($file_data['filesize'])) {
      $file['filesize'] = (int) $file_data['filesize'];
    }
    if (!empty($file_data['alt'])) {
      $file['alt'] = $file_data['alt'];
    }
    if (!empty($file_data['title'])) {
      $file['title'] = $file_data['title'];
    }
    if (!empty($file_data['width'])) {
      $file['width'] = $file_data['width'];
    }
    if (!empty($file_data['height'])) {
      $file['height'] = $file_data['height'];
    }
    // Now we save.
    $create = $fileStorage->create($file);
    $create->enforceIsNew();
    try {
      $create->save();
      /** @var \Drupal\file\Entity\File $file_entity */
      $file_entity = $fileStorage->load($create->id());
      return $file_entity;
    } catch (Drupal\Core\Entity\EntityStorageException $e) {
      echo t(
          'Archport - copy_create_imported_file - Failed to save file. Exception: '
        ) . $e->getMessage() . "\n";
      return FALSE;
    }
  }

}
