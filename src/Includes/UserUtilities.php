<?php
/** @file Archport functions dealing with users. */

namespace Drupal\archport\Includes;

use Drupal;

class UserUtilities {

  /**
   * Load a user by their uid, then return a custom array with the loaded data.
   *
   * @param int $uid
   *
   * @return array|false
   */
  public static function _archport_load_user(int $uid): bool|array {
    try {
      /** @var \Drupal\user\UserStorage $userStorage */
      $userStorage = Drupal::entityTypeManager()->getStorage('user');
    } catch (Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException|Drupal\Component\Plugin\Exception\PluginNotFoundException $e) {
      echo t(
          'Failed to get entity storage class. Exception: '
        ) . $e->getMessage() . "\n";
      return FALSE;
    }
    /** @var \Drupal\user\Entity\User $user */
    $user = $userStorage->load($uid);
    if (!$user) {
      echo t("Failed to load user object\n");
      return FALSE;
    }

    return [
      'uid' => $uid,
      'username' => $user->getAccountName(),
      'email' => $user->getEmail(),
      'created' => $user->getCreatedTime(),
      'updated' => $user->getChangedTime(),
    ];
  }

  /**
   * Create a new user based on the data found in map.json.
   *
   * @param array $mappedUser
   *
   * @return bool
   */
  public static function _archport_create_user(array $mappedUser): bool {
    try {
      /** @var \Drupal\user\UserStorage $userStorage */
      $userStorage = Drupal::entityTypeManager()->getStorage('user');
    } catch (Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException|Drupal\Component\Plugin\Exception\PluginNotFoundException $e) {
      echo t(
          'Failed to get entity storage class. Exception: '
        ) . $e->getMessage() . "\n";
      return FALSE;
    }
    $newAccount = [];
    if (isset($mappedUser['target_username'])) {
      $newAccount['name'] = $mappedUser['target_username'];
    }
    else {
      $newAccount['name'] = $mappedUser['source_username'];
    }
    if (isset($mappedUser['target_created'])) {
      $newAccount['created'] = $mappedUser['target_created'];
    }
    else {
      $newAccount['created'] = $mappedUser['source_created'];
    }
    if (isset($mappedUser['target_email'])) {
      $newAccount['mail'] = $mappedUser['target_email'];
    }
    else {
      $newAccount['mail'] = $mappedUser['source_email'];
    }
    if (isset($mappedUser['target_changed'])) {
      $newAccount['changed'] = $mappedUser['target_changed'];
    }
    else {
      $newAccount['changed'] = $mappedUser['source_changed'];
    }
    $newAccount['is_new'] = TRUE;
    $create = $userStorage->create($newAccount);
    try {
      $create->save();
      return TRUE;
    } catch (Drupal\Core\Entity\EntityStorageException $e) {
      echo t(
          'Archport - create_user - Failed to save new user. Exception: '
        ) . $e->getMessage() . "\n";
      return FALSE;
    }
  }

  /**
   * If the settings say to do so, loop through users in map.json data and
   * import them.
   *
   * @param array $map
   * @param array $settings
   *
   * @return bool
   */
  public static function _archport_import_users(
    array $map,
    array $settings
  ): bool {
    try {
      $userStorage = Drupal::entityTypeManager()->getStorage('user');
    } catch (Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException|Drupal\Component\Plugin\Exception\PluginNotFoundException $e) {
      echo t(
          'Failed to get entity storage class. Aborting. Exception: '
        ) . $e->getMessage() . "\n";
      return FALSE;
    }
    if (!$settings['create_users']) {
      // Just skip since creating users is false.
      return TRUE;
    }
    if (empty($map['users'])) {
      return TRUE;
    }

    foreach ($map['users'] as $user_data) {
      if ($user_data['source_uid'] === 0 || $user_data['source_uid'] === "0") {
        // Anonymous user. Skip.
        continue;
      }
      if ($user_data['source_uid'] === 1 || $user_data['source_uid'] === "1") {
        // Superadmin user. Skip.
        continue;
      }
      if (!empty($user_data['target_uid'])) {
        // User already mapped to existing user. Skip
        continue;
      }
      $new_user_data = [];
      if ($settings['keep_created']) {
        if (!empty($user_data['target_created'])) {
          $new_user_data['created'] = $user_data['target_created'];
        }
        elseif (!empty($user_data['source_created'])) {
          $new_user_data['created'] = $user_data['source_created'];
        }// Else skip it and let Drupal set it.
      }
      if ($settings['keep_updated']) {
        // This probably won't actually work since Drupal will override it...
        if (!empty($user_data['target_updated'])) {
          $new_user_data['changed'] = $user_data['target_updated'];
        }
        elseif (!empty($user_data['source_created'])) {
          $new_user_data['changed'] = $user_data['source_updated'];
        }// Else skip it and let Drupal set it.
      }
      if (!empty($user_data['target_email'])) {
        $new_user_data['mail'] = $user_data['target_email'];
      }
      elseif (!empty($user_data['source_email'])) {
        $new_user_data['mail'] = $user_data['source_email'];
      }// Else skip it and let Drupal set it.

      if (!empty($user_data['target_username'])) {
        $new_user_data['name'] = $user_data['target_username'];
      }
      elseif (!empty($user_data['source_username'])) {
        $new_user_data['name'] = $user_data['source_username'];
      }// Else skip it and let Drupal set it.

      // Does the user already exist?
      $userLoad = $userStorage->loadByProperties(
        ['name' => $new_user_data['name']]
      );
      if (empty($userLoad)) {
        echo t(
            'Archport - import_users - Try to import user with username: '
          ) . $new_user_data['name'] . "\n";
        $create = $userStorage->create($new_user_data);
        $create->enforceIsNew();
        try {
          $create->save();
          echo t(
              'Archport - import_users - Succeeded in importing user with username: '
            ) . $new_user_data['name'] . "\n";
        } catch (Drupal\Core\Entity\EntityStorageException $e) {
          echo t(
              'Archport - import_users - Failed to save new user. Aborting. Exception: '
            ) . $e->getMessage() . "\n";
          return FALSE;
        }
      }// Else the user exists, so just move on to the next one.
    }
    return TRUE;
  }

}
