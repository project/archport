<?php
/** @file Archport utility functions. */

namespace Drupal\archport\Includes;

use Drupal;

class Utilities {

  /**
   * Prepare the export directory for a node based on its type and id.
   *
   * @param $archport_directory
   * @param $type
   * @param $id
   *
   * @return false|string
   */
  public static function _archport_prepare_node_dir(
    $archport_directory,
    $type,
    $id
  ): bool|string {
    $directory = $archport_directory . '/' . $type . '/' . $id;
    return Utilities::_archport_prepare_directory($directory);
  }

  /**
   * Create the directory if it does not exist. Uses mode 0770, and is
   * recursive.
   *
   * @param $directory
   *
   * @return bool|string
   */
  public static function _archport_prepare_directory($directory): bool|string {
    /** @var \Drupal\Core\File\FileSystem $fileSystem */
    $fileSystem = Drupal::service('file_system');
    if (!file_exists($directory)) {
      $prepare_directory = $fileSystem->mkdir($directory, 0770, TRUE);
      if (!$prepare_directory) {
        echo t(
            "Archport - prepare_dir - Failed to prepare directory: "
          ) . $directory . "\n";
        return FALSE;
      }
    }
    return $directory;
  }

  /**
   * Get all the field names for node entity types.
   *
   * @return array|bool
   */
  public static function _archport_get_all_node_field_names(): array|bool {
    try {
      /** @var \Drupal\field\FieldStorageConfigStorage $field_storage_config */
      $field_storage_config = Drupal::entityTypeManager()->getStorage(
        'field_storage_config'
      );
    } catch (Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException|Drupal\Component\Plugin\Exception\PluginNotFoundException $e) {
      echo t(
          'Failed to get entity storage class. Exception: '
        ) . $e->getMessage() . "\n";
      return FALSE;
    }
    $load_field_configs = $field_storage_config->loadMultiple();
    $field_names = [];
    /** @var \Drupal\field\Entity\FieldStorageConfig $config */
    foreach ($load_field_configs as $name => $config) {
      if (str_contains($name, 'node.')) {
        $field_names[] = $config->get('field_name');
      }
    }
    return $field_names;
  }

  /**
   * Get all the node entity types.
   *
   * @return array
   */
  public static function _archport_get_all_node_entity_types(): array {
    $defs = Drupal::service('entity_type.bundle.info')->getBundleInfo('node');
    return array_keys($defs);
  }

  /**
   * Returns the first section of a given uri. "public://test/foo/bar" will
   * return "public://test".
   *
   * @param $uri
   *
   * @return string
   */
  public static function _archport_get_archport_directory_from_uri($uri) {
    $target = FileUtilities::_archport_get_uri_target($uri);
    $scheme = FileUtilities::_archport_get_uri_scheme($uri);
    $explode = explode('/', $target);
    return $scheme . '://' . $explode[0];
  }

}
