<?php
/** @file Archport functions dealing with settings.json. */

namespace Drupal\archport\Includes;

use Drupal;
use Drupal\Core\File\FileSystemInterface;

class SettingsUtilities {

  /**
   * If the file doesn't exist already, write the initial settings array to
   * the destination in $dest.
   *
   * @param $archport_directory
   *
   * @return bool
   */
  public static function _archport_write_initial_settings($archport_directory
  ): bool {
    /** @var \Drupal\Core\File\FileSystem $fileSystem */
    $fileSystem = Drupal::service('file_system');
    if (file_exists($archport_directory)) {
      if (!file_exists($archport_directory . '/settings.json')) {
        $settings = [
          'source_site_base_url' => '',
          'target_site_base_url' => '',
          'write_apache_redirects' => FALSE,
          'write_haproxy_redirects' => FALSE,
          'keep_created' => TRUE,
          'keep_updated' => TRUE,
          'skip_vocabularies' => FALSE,
          'skip_terms' => FALSE,
          'create_users' => TRUE,
          'create_vocabularies' => TRUE,
          'create_terms' => TRUE,
          'create_aliases' => TRUE,
          'create_nid_aliases' => FALSE,
          'override_file_paths' => [
            'public' => [
              'override' => FALSE,
              'existing_action' => 'rename',
              'path' => 'public://',
            ],
            'private' => [
              'override' => FALSE,
              'existing_action' => 'rename',
              'path' => 'private://',
            ],
            'unknown' => [
              'override' => FALSE,
              'existing_action' => 'rename',
              'path' => 'private://unknown',
            ],
          ],
        ];
        $settingsJson = json_encode($settings, JSON_PRETTY_PRINT);
        $save = $fileSystem->saveData(
          $settingsJson,
          $archport_directory . '/settings.json',
          FileSystemInterface::EXISTS_REPLACE
        );
        if (!$save) {
          echo
          t(
            "Archport - write_initial_settings - Failed to save initial settings.json.\n"
          );
          return FALSE;
        }
        else {
          echo
          t(
            "Archport - write_initial_settings - Saved initial settings.json.\n"
          );
          return TRUE;
        }
      }
      else {
        echo t(
          "Archport - write_initial_settings - settings.json already exists. Please manually update it if you need to.\n"
        );
        return TRUE;
      }
    }
    else {
      echo
      t(
        "Archport - write_initial_settings - The root Archport destination directory does not exist. This public static function should only be run after that directory is created.\n"
      );
      return FALSE;
    }
  }

  /**
   * Read the settings.json file at $source into an array.
   *
   * @param string $archport_directory
   *
   * @return false|array
   */
  public static function _archport_read_settings(string $archport_directory
  ): false|array {
    if (file_exists($archport_directory . '/settings.json')) {
      $settingsJson = file_get_contents($archport_directory . '/settings.json');
      $settings = json_decode($settingsJson, TRUE);
      if (!$settings) {
        echo
        'Archport - read_settings - The settings.json file contains invalid json.';
        return FALSE;
      }
      else {
        return $settings;
      }
    }
    else {
      echo t(
        "Archport - read_settings - The settings.json file does not exist.\n"
      );
      return FALSE;
    }
  }

}
