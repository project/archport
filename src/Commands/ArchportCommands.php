<?php

namespace Drupal\archport\Commands;

use Drupal;
use Drupal\archport\Includes\FileUtilities;
use Drupal\archport\Includes\MapUtilities;
use Drupal\archport\Includes\MediaUtilities;
use Drupal\archport\Includes\NodeUtilities;
use Drupal\archport\Includes\SettingsUtilities;
use Drupal\archport\Includes\TaxonomyUtilities;
use Drupal\archport\Includes\UserUtilities;
use Drupal\archport\Includes\Utilities;
use Drush\Commands\DrushCommands;
use InvalidArgumentException;

class ArchportCommands extends DrushCommands {

  /**
   * @param string $archport_directory URI to where you want to save exported
   *                                   data
   *
   * @command archport:initialize
   * @usage   archport:initialize private://archport
   */
  public function initialize(string $archport_directory): bool {
    $prepare_target_directory = Utilities::_archport_prepare_directory(
      $archport_directory
    );
    if (!$prepare_target_directory) {
      echo t(
          'Archport - initialize - Failed to prepare target directory. Aborting.'
        ) . "\n";
      return FALSE;
    }
    $write_settings = SettingsUtilities::_archport_write_initial_settings(
      $archport_directory
    );
    if (!$write_settings) {
      echo t(
          'Archport - initialize - Failed to write initial settings.json. Aborting.'
        ) . "\n";
      return FALSE;
    }
    $write_map = $this->generateMap($archport_directory);
    if (!$write_map) {
      echo t(
          'Archport - initialize - Failed to write initial map.json. Aborting.'
        ) . "\n";
      return FALSE;
    }
    echo t(
      'Archport - initialize - Succeeded. Please go edit your map.json and settings.json files.'
    );
    return TRUE;
  }

  /**
   * Generates the map.json file by scanning all data.json files in the given
   * directory.
   *
   * @param string $archport_directory A file uri to the archport directory.
   *                                   Like private://archport
   *
   * @command archport:generate-map
   * @usage   archport:generate-map private://archport
   */
  public function generateMap(
    string $archport_directory
  ): bool {
    $map = MapUtilities::_archport_read_map($archport_directory);
    if ($map === FALSE) {
      echo t(
          'Archport - generate_map - map.json file did not exist and could not be created. Aborting.'
        ) . "\n";
      return FALSE;
    }
    /** @var Drupal\Core\File\FileSystem $fileSystem */
    $fileSystem = Drupal::service('file_system');
    $data_json_files = $fileSystem->scanDirectory(
      $archport_directory,
      '$data\.json$'
    );
    foreach ($data_json_files as $file_info) {
      $data = json_decode(file_get_contents($file_info->uri), TRUE);
      MapUtilities::_archport_write_single_node_map(
        $archport_directory,
        $data
      );
    }
    $map = MapUtilities::_archport_read_map($archport_directory);
    if ($map === FALSE) {
      echo t(
          'Archport - generate_map - Failed to read map array from target file. Aborting.'
        ) . "\n";
      return FALSE;
    }
    if (!isset($map['content_types'])) {
      $map['content_types'] = [];
    }
    $content_types = Utilities::_archport_get_all_node_entity_types();
    foreach ($content_types as $type) {
      if (!array_key_exists($type, $map['content_types'])) {
        $map['content_types'][$type] = '';
      }
    }
    if (!isset($map['field_types'])) {
      $map['field_types'] = [];
    }
    $fields = Utilities::_archport_get_all_node_field_names();
    if (!array_key_exists('field_types', $map)) {
      $map['field_types'] = [];
    }
    if (!isset($map['fields'])) {
      $map['fields'] = [];
    }
    foreach ($fields as $field) {
      if (!array_key_exists($field, $map['fields'])) {
        $map['fields'][$field] = '';
      }
      if (!array_key_exists($field, $map['field_types'])) {
        $map['field_types'][$field] = '';
      }
    }
    $write = MapUtilities::_archport_write_map_file($map, $archport_directory);
    if (!$write) {
      echo t(
          'Archport - generate_map - Failed to write map array to target file.'
        ) . "\n";
      return FALSE;
    }
    return TRUE;
  }

  /**
   * Loops though users in map.json and creates them in Drupal. Updates
   * map.json with the new target_* info for the imported users.
   *
   * @param string $archport_directory A file uri to the archport directory.
   *                                   Like private://archport
   *
   * @command archport:import-users
   * @usage   archport:import-users private://archport
   */
  public function importUsers($archport_directory) {
    // Check if things have been initialized.
    if (!file_exists($archport_directory) || !file_exists(
        $archport_directory . '/map.json'
      ) || !file_exists($archport_directory . '/settings.json')) {
      echo t(
          'Archport - import_users - Destination directory, map.json, and/or settings.json do not exist. Please run archport:initialize and then edit map.json and settings.json before continuing.'
        ) . "\n";
      return FALSE;
    }
    // We need the map in order to deal with how we export data.
    $map = MapUtilities::_archport_read_map($archport_directory);
    if ($map === FALSE) {
      echo t(
          'Archport - import_users - Failed to read map.json. Please check your destination file and folder permissions. Aborting.'
        ) . "\n";
      return FALSE;
    }
    $settings = SettingsUtilities::_archport_read_settings($archport_directory);
    if ($settings === FALSE) {
      echo t(
          'Archport - import_users - Failed to read settings.json. Please check your destination file and folder permissions. Aborting.'
        ) . "\n";
      return FALSE;
    }
    $import_result = UserUtilities::_archport_import_users($map, $settings);
    if ($import_result) {
      // Let's get the new user id's and update map.json.
      try {
        $userStorage = Drupal::entityTypeManager()->getStorage('user');
      } catch (Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException|Drupal\Component\Plugin\Exception\PluginNotFoundException $e) {
        echo t(
            'Failed to get entity storage class. Aborting. Exception: '
          ) . $e->getMessage() . "\n";
        return FALSE;
      }
      foreach ($map['users'] as $key => $user) {
        if ($user['source_uid'] === 0 || $user['source_uid'] === "0") {
          // Anonymous user. Skip.
          continue;
        }
        if ($user['source_uid'] === 1 || $user['source_uid'] === "1") {
          // Superadmin user. Skip.
          continue;
        }
        if (!empty($user_data['target_uid'])) {
          // User already mapped to existing user. Skip
          continue;
        }
        $userLoad = $userStorage->loadByProperties(
          ['name' => $user['source_username']]
        );
        if (!empty($userLoad)) {
          /** @var \Drupal\user\Entity\User $user_object */
          $user_object = array_pop($userLoad);
          if (empty($map['users'][$key]['target_uid'])) {
            $map['users'][$key]['target_uid'] = $user_object->id();
          }
          if (empty($map['users'][$key]['target_username'])) {
            $map['users'][$key]['target_username'] = $user['source_username'];
          }
          if (empty($map['users'][$key]['target_email'])) {
            $map['users'][$key]['target_email'] = $user_object->getEmail();
          }
          if (empty($map['users'][$key]['target_created'])) {
            $map['users'][$key]['target_created'] = $user_object->getCreatedTime();
          }
          if (empty($map['users'][$key]['target_updated'])) {
            $map['users'][$key]['target_updated'] = $user_object->getChangedTime();
          }
        }
      }
      MapUtilities::_archport_write_map_file($map, $archport_directory);
    }
    return TRUE;
  }

  /**
   * Exports all nodes of a content type into the Archport format.
   *
   * @param string $archport_directory A valid file uri like private://archport
   * @param string $type A valid content type machine name.
   *
   * @command archport:export-content-type
   * @usage   archport:export-content-type blog private://archport
   */
  public function exportContentType(
    string $archport_directory,
    string $type
  ): bool {
    // Check if things have been initialized.
    if (!file_exists($archport_directory) || !file_exists(
        $archport_directory . '/map.json'
      ) || !file_exists($archport_directory . '/settings.json')) {
      echo t(
          'Archport - export_content_type - Destination directory, map.json, and/or settings.json do not exist. Please run archport:initialize and then edit map.json and settings.json before continuing.'
        ) . "\n";
      return FALSE;
    }
    // We need the map in order to deal with how we export data.
    $map = MapUtilities::_archport_read_map($archport_directory);
    if ($map === FALSE) {
      echo t(
          'Archport - export_content_type - Failed to read map.json. Please check your destination file and folder permissions. Aborting.'
        ) . "\n";
      return FALSE;
    }
    if (!array_key_exists($type, $map['content_types'])) {
      echo t(
          'Archport - export_content_type - Given type is unknown. Please select a valid type, and/or add it to the map.json content_types array. Aborting.'
        ) . "\n";
      return FALSE;
    }
    try {
      /** @var \Drupal\node\NodeStorage $nodeStorage */
      $nodeStorage = Drupal::entityTypeManager()->getStorage('node');
    } catch (Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException|Drupal\Component\Plugin\Exception\PluginNotFoundException $e) {
      echo t(
          'Failed to get entity storage class. Exception: '
        ) . $e->getMessage() . "\n";
      return FALSE;
    }
    $nodes = $nodeStorage->loadByProperties(['type' => $type]);

    if (!empty($nodes)) {
      echo t(
          'Archport - export_content_type - Found '
        ) . count($nodes) . t(' nodes of type ') . $type . t(
          ' to export.'
        ) . "\n";
      $ids = array_keys($nodes);
      foreach ($ids as $id) {
        $this->exportSingleNode($archport_directory, $id);
      }
    }
    else {
      echo t(
          'Archport - export_content_type - Failed to load any nodes of type '
        ) . $type . t(' for export.') . "\n";
      return FALSE;
    }
    return TRUE;
  }

  /**
   * Saves a node, and its fields, into the Archport format.
   *
   * @param string $archport_directory A valid file uri like private://archport
   * @param int $id A valid entity id number.
   *
   * @command archport:export-single-node
   *
   * @usage   archport:export-single-node private://archport 134
   */
  public function exportSingleNode(string $archport_directory, int $id): bool {
    // Check if things have been initialized.
    if (!file_exists($archport_directory) || !file_exists(
        $archport_directory . '/map.json'
      ) || !file_exists($archport_directory . '/settings.json')) {
      echo t(
          'Archport - export_single_node - Destination directory, map.json, and/or settings.json do not exist. Please run archport:initialize and then edit map.json and settings.json before continuing.'
        ) . "\n";
      return FALSE;
    }
    // We need the map in order to deal with how we export data.
    $map = MapUtilities::_archport_read_map($archport_directory);
    if ($map === FALSE) {
      echo t(
          'Archport - export_single_node - Failed to read map.json. Please check your destination file and folder permissions. Aborting.'
        ) . "\n";
      return FALSE;
    }
    // All the preliminaries passed. On to the meat of the operation.
    echo t('Archport - export_node - Exporting node id ') . $id . t(
        ' to destination directory: '
      ) . $archport_directory . "\n";


    try {
      /** @var \Drupal\node\NodeStorage $nodeStorage */
      $nodeStorage = Drupal::entityTypeManager()->getStorage('node');
    } catch (Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException|Drupal\Component\Plugin\Exception\PluginNotFoundException $e) {
      echo t(
          'Failed to get entity storage class. Exception: '
        ) . $e->getMessage() . "\n";
      return FALSE;
    }
    /** @var \Drupal\node\Entity\Node $current_node */
    $current_node = $nodeStorage->load($id);

    if (empty($current_node)) {
      echo t("Archport - export_node - Failed to load node. Aborting.") . "\n";
      return FALSE;
    }

    // Prepare the node destination subdirectory, no sense continuing if this
    // doesn't work.
    $export_node_to_directory = Utilities::_archport_prepare_node_dir(
      $archport_directory,
      $current_node->getType(),
      $id
    );
    if (!$export_node_to_directory) {
      echo t(
          'Archport - export_single_node - Node subdirectory could did not exist and could not be created. Aborting.'
        ) . "\n";
      return FALSE;
    }

    // Start $node array.
    $node['nid'] = $id;
    $node['uid'] = $current_node->getOwnerId();
    $node['title'] = $current_node->getTitle();
    $node['status'] = $current_node->isPublished();
    $node['type'] = $current_node->getType();
    $node['created'] = $current_node->getCreatedTime();
    $node['changed'] = $current_node->getChangedTime();
    $alias = \Drupal::service('path_alias.manager')->getAliasByPath(
      '/node/' . $id
    );
    if ($alias) {
      $node['alias'] = $alias;
    }

    // Load the body if it is set.
    $bodyField = $current_node->get('body');

    try {
      $body = $bodyField->first()->getValue();
    } catch (Drupal\Core\TypedData\Exception\MissingDataException $e) {
      echo t(
          'Archport - export_single_node - Failed to get node body value. Aborting. Exception thrown: '
        ) . $e->getMessage() . "\n";
      return FALSE;
    }
    if (!empty($body)) {
      // Note that _safe fields are leftover from Drupal 7.
      $node['body'] = $body['value'];
      $node['body_format'] = $body['format'];
      $node['body_safe'] = $body['value'];
      // Load the summary only if it is set, and is not empty.
      if (!empty($body['summary'])) {
        $node['body_summary'] = $body['summary'];
        $node['body_summary_safe'] = $body['summary'];
      }
    }
    else {
      echo t(
          'Archport - export_single_node - Failed to get body field. Aborting'
        ) . "\n";
      return FALSE;
    }

    $user = UserUtilities::_archport_load_user($current_node->getOwnerId());
    if (!$user) {
      echo t(
          'Archport - export_single_node - Failed to load node owner. Aborting.'
        ) . "\n";
      return FALSE;
    }

    // Deal with fields.
    // Fields will contain any taxonomies, so initiate that.
    $taxonomies = [];
    $terms = []; // We will process these into $taxonomies.

    // Fields will contain any files, so initiate that.
    $files = [];

    // Media will contain any media items.
    $media = [];

    // And the rest go here.
    $fields = [];

    // Find fields
    $known_fields = $map['field_types'];
    foreach ($known_fields as $field_name => $field_kind) {
      // Skip body field if it is in the array of known fields.
      if ($field_name === 'body') {
        continue;
      }
      try {
        $field_item_list = $current_node->get($field_name);
      } catch (InvalidArgumentException $e) {
        echo t(
            'Archport - export_single_node - Tried to get field from a node that doesn\'t have it. Exception: '
          ) . $e->getMessage() . "\n";
        continue;
      }
      if (!empty($field_item_list)) {
        // Use a for loop since FieldItemList uses the get() method based
        // to retrieve the actual field data. Can't just foreach it.
        for ($i = 0; $i < $field_item_list->count(); $i++) {
          try {
            $item = $field_item_list->get($i);
          } catch (Drupal\Core\TypedData\Exception\MissingDataException $e) {
            echo t(
                'Archport - export_single_node - Failed to get item from field item list. Aborting. Exception: '
              ) . $e->getMessage() . "\n";
            return FALSE;
          }
          $value = $item->getValue();
          if ($field_kind === 'term') {
            $termLoad = TaxonomyUtilities::_archport_load_term(
              $value['target_id']
            );
            if (!empty($termLoad)) {
              $terms[] = array_merge_recursive(
                $termLoad,
                ['field_name' => $field_name]
              );
            }
          }
          elseif ($field_kind === 'image') {
            $file = FileUtilities::_archport_load_file_by_target_id(
              $value['target_id']
            );
            $files[] = array_merge_recursive($value, [
              'field_name' => $field_name,
              'file' => $file,
            ]);
          }
          elseif ($field_kind === 'media') {
            $mediaLoad = MediaUtilities::_archport_load_media_by_target_id(
              $value['target_id']
            );
            $media[] = [
              'field_name' => $field_name,
              'target_id' => $value['target_id'],
              'media' => $mediaLoad,
            ];
          }
          elseif ($field_kind === 'text') {
            $fields[] = array_merge_recursive(
              $value,
              ['field_name' => $field_name]
            );
          }
        }
      }
    }

    // Now that we have found any vocabularies and terms, parse the data into $taxonomies.
    foreach ($terms as $term) {
      $taxonomies[$term['vocabulary']['vid']]['vid'] = $term['vocabulary']['vid'];
      $taxonomies[$term['vocabulary']['vid']]['name'] = $term['vocabulary']['name'];
      $taxonomies[$term['vocabulary']['vid']]['machine_name'] = $term['vocabulary']['machine_name'];
      $taxonomies[$term['vocabulary']['vid']]['description'] = $term['vocabulary']['description'];
      $taxonomies[$term['vocabulary']['vid']]['hierarchy'] = $term['vocabulary']['hierarchy'];
      $taxonomies[$term['vocabulary']['vid']]['weight'] = $term['vocabulary']['weight'];
      $taxonomies[$term['vocabulary']['vid']]['terms'][$term['tid']]['tid'] = $term['tid'];
      $taxonomies[$term['vocabulary']['vid']]['terms'][$term['tid']]['name'] = $term['name'];
      $taxonomies[$term['vocabulary']['vid']]['terms'][$term['tid']]['description'] = $term['description'];
      $taxonomies[$term['vocabulary']['vid']]['terms'][$term['tid']]['weight'] = $term['weight'];
      $taxonomies[$term['vocabulary']['vid']]['terms'][$term['tid']]['field_name'] = $term['field_name'];
    }

    $processed_files = [];
    if (!empty($files)) {
      // Process normal file field data.
      $processed_files = FileUtilities::_archport_process_exported_files(
        $export_node_to_directory,
        $files
      );
    }

    $processed_media = [];
    if (!empty($media)) {
      // Process media entities.
      $processed_media = MediaUtilities::_archport_process_exported_media(
        $export_node_to_directory,
        $media
      );
      if (empty($processed_media)) {
        $processed_media = [];
      }
    }

    // Put all the data together.
    $data = [
      'node' => $node,
      'users' => $user,
      'taxonomies' => $taxonomies,
      'files' => $processed_files,
      'fields' => $fields,
      'media' => $processed_media,
    ];

    $file_path = $export_node_to_directory . '/data.json';

    /** @var Drupal\Core\File\FileSystem $fileSystem */
    $fileSystem = Drupal::service('file_system');
    $save_file = $fileSystem->saveData(
      json_encode($data, JSON_PRETTY_PRINT),
      $file_path,
      Drupal\Core\File\FileSystemInterface::EXISTS_REPLACE
    );

    // Look at the resulting data array and add anything new to map.json.
    $write_map = MapUtilities::_archport_write_single_node_map(
      $archport_directory,
      $data
    );

    if (!$save_file) {
      echo t(
          "Archport - export_single_node - Failed to save node data.json file."
        ) . "\n";
    }
    else {
      echo t(
          "Archport - export_single_node - Succeeded in saving node data.json file."
        ) . "\n";
      echo 'Filepath: ' . $save_file . "\n";
    }
    if (!$save_file || !$write_map) {
      return FALSE;
    }
    else {
      echo t(
          'Archport - export_single_node - Export of node complete. Remember to set target user id\'s in map.json if you don\'t want the imported nodes to be owned by anonymous.'
        ) . "\n";
      return $save_file;
    }
  }

  /**
   * Exports all nodes on the site into the Archport format.
   *
   * @param string $archport_directory A valid file uri like private://archport
   *
   * @command archport:export-all-nodes
   * @usage   archport:export-all-nodes private://archport
   */
  public function exportAllNodes(string $archport_directory): bool {
    // Check if things have been initialized.
    if (!file_exists($archport_directory) || !file_exists(
        $archport_directory . '/map.json'
      ) || !file_exists($archport_directory . '/settings.json')) {
      echo t(
          'Archport - export_all_nodes - Destination directory, map.json, and/or settings.json do not exist. Please run archport:initialize and then edit map.json and settings.json before continuing.'
        ) . "\n";
      return FALSE;
    }
    // We need the map in order to deal with how we export data.
    $map = MapUtilities::_archport_read_map($archport_directory);
    if ($map === FALSE) {
      echo t(
          'Archport - export_all_nodes - Failed to read map.json. Please check your destination file and folder permissions. Aborting.'
        ) . "\n";
      return FALSE;
    }

    try {
      /** @var \Drupal\node\NodeStorage $nodeStorage */
      $nodeStorage = Drupal::entityTypeManager()->getStorage('node');
    } catch (Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException|Drupal\Component\Plugin\Exception\PluginNotFoundException $e) {
      echo t(
          'Failed to get entity storage class. Exception: '
        ) . $e->getMessage() . "\n";
      return FALSE;
    }
    $nodes = $nodeStorage->loadMultiple();
    if (!empty($nodes)) {
      echo t(
          'Archport - export_all_nodes - Found '
        ) . count(
          $nodes
        ) . t(
          ' nodes to export.'
        ) . "\n";
      $ids = array_keys($nodes);
      foreach ($ids as $id) {
        $this->exportSingleNode($archport_directory, $id);
      }
    }
    else {
      echo t(
          'Archport - export_all_nodes - Failed to load any nodes for export.'
        ) . "\n";
      return FALSE;
    }
    return TRUE;
  }

  /**
   * Imports an entity from the Archport format into Drupal.
   *
   * @param string $archport_directory A valid file uri like private://archport
   * @param int $id A valid entity id number.
   * @param string $type The machine name of a content type.
   *
   * @command archport:import-single-node
   * @usage   archport:import-single-node private://archport 134 page
   */
  public function importSingleNode(
    string $archport_directory,
    int $id,
    string $type
  ): bool {
    $settings = SettingsUtilities::_archport_read_settings($archport_directory);
    $map = MapUtilities::_archport_read_map($archport_directory);
    $data = NodeUtilities::_archport_read_node_data(
      $archport_directory,
      $id,
      $type
    );
    if (!$data) {
      return FALSE;
    }
    $import_vocabularies = TaxonomyUtilities::_archport_import_vocabularies(
      $map,
      $settings
    );
    if (!$import_vocabularies) {
      echo t(
        "Archport - import_node - Vocabulary import failed, check for log messages.\n"
      );
      return FALSE;
    }

    $import_terms = TaxonomyUtilities::_archport_import_terms($map, $settings);
    if (!$import_terms) {
      echo t(
        "Archport - import_node - Term import failed, check for log messages.\n"
      );
      return FALSE;
    }

    // _archport_import_node() will also deal with files and fields.
    $source_directory = $archport_directory . '/' . $type . '/' . $id;
    $import_node = NodeUtilities::_archport_import_node(
      $source_directory,
      $data,
      $settings,
      $map
    );
    if (!$import_node) {
      echo t(
        "Archport - import_node - Node import failed, check for log messages.\n"
      );
      return FALSE;
    }
    return TRUE;
  }

  /**
   * Imports all nodes of a content type from the Archport format into Drupal.
   *
   * @param string $archport_directory A valid file uri like private://archport
   * @param string $type A valid content type machine name.
   *
   * @command archport:import-content-type
   * @usage   archport:import-content-type private://archport blog
   */
  public function importContentType(
    string $archport_directory,
    string $type
  ): bool {
    $source_directory = $archport_directory . '/' . $type;
    if (file_exists($source_directory)) {
      /** @var Drupal\Core\File\FileSystem $fileSystem */
      $fileSystem = Drupal::service('file_system');
      $data_json_files = $fileSystem->scanDirectory(
        $source_directory,
        '$data\.json$'
      );
      if (!empty($data_json_files) && is_array($data_json_files)) {
        foreach ($data_json_files as $data_json_file) {
          $uri = $fileSystem->dirname($data_json_file->uri);
          $explode = explode('/', $uri);
          $id = $explode[count($explode) - 1];
          $import_result = $this->importSingleNode(
            $archport_directory,
            (int) $id,
            $type
          );
          if (!$import_result) {
            echo t(
                'Archport - import_content_type - Failed to import node. Aborting. Content type and node id were: '
              ) . ' Type: ' . $type . ' ID: ' . $id . "\n";
            return FALSE;
          }
        }
      }
      else {
        echo t(
            'Archport - import_content_type - Failed to import content type. Something went wrong when looking for data.json files. Directory checked: '
          ) . $source_directory . "\n";
        return FALSE;
      }
    }
    else {
      echo t(
          'Archport - import_content_type - Failed to import content type. Source directory does not exist. Directory checked: '
        ) . $source_directory . "\n";
      return FALSE;
    }
    return TRUE;
  }

  /**
   * Imports all nodes in the source directory from the Archport format into
   * Drupal.
   *
   * @param string $archport_directory A valid file uri like private://archport
   *
   * @command archport:import-all-nodes
   * @usage   archport:import-all-nodes private://archport
   */
  public function importAllNodes(string $archport_directory): bool {
    if (file_exists($archport_directory)) {
      /** @var Drupal\Core\File\FileSystem $fileSystem */
      $fileSystem = Drupal::service('file_system');
      $data_json_files = $fileSystem->scanDirectory(
        $archport_directory,
        '$data\.json$'
      );
      if (!empty($data_json_files) && is_array($data_json_files)) {
        foreach ($data_json_files as $data_json_file) {
          $uri = $fileSystem->dirname($data_json_file->uri);
          $explode = explode('/', $uri);
          $id = $explode[count($explode) - 1];
          $type = $explode[count($explode) - 2];
          $import_result = $this->importSingleNode(
            $archport_directory,
            $id,
            $type
          );
          if (!$import_result) {
            echo t(
                'Archport - import_all_nodes - Failed to import node. Aborting. Content type and node id were: '
              ) . ' Type: ' . $type . ' ID: ' . $id . "\n";
            return FALSE;
          }
        }
      }
      else {
        echo t(
            'Archport - import_all_nodes - Failed to import nodes. Something went wrong when looking for data.json files. Directory checked: '
          ) . $archport_directory . "\n";
        return FALSE;
      }
    }
    else {
      echo t(
          'Archport - import_all_nodes - Failed to import nodes. Source directory does not exist. Directory checked: '
        ) . $archport_directory . "\n";
      return FALSE;
    }
    return TRUE;
  }

}
